/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg, Margaret Millhouse
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

//TODO: Amplitude prior and proposals need to call same functions!

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "BayesWave.h"
#include "BayesLine.h"
#include "BayesWaveMath.h"
#include "BayesWavePrior.h"
#include "BayesWaveWavelet.h"

void set_bayesline_priors(char *channel, struct BayesLineParams *bayesline, double Tobs)
{
  //S6 strain
  if(!strcmp(channel,"LDAS-STRAIN"))
  {
    bayesline->priors->SAmin = 1.0e-48;
    bayesline->priors->SAmax = 1.0e-25;
    bayesline->priors->LQmin = 1.0e2;
    bayesline->priors->LQmax = 1.0e8;
    bayesline->priors->LAmin = 1.0e-44;
    bayesline->priors->LAmax = 1.0e-20;
  }

  //VSR2 strain
  else if (!strcmp(channel,"h_16384Hz"))
  {
    bayesline->priors->SAmin = 1.0e-48;
    bayesline->priors->SAmax = 1.0e-30;
    bayesline->priors->LQmin = 1.0e2;
    bayesline->priors->LQmax = 1.0e8;
    bayesline->priors->LAmin = 1.0e-44;
    bayesline->priors->LAmax = 1.0e-30;
  }

  //S6 LIGO DARM Error
  else if (!strcmp(channel,"LSC-DARM_ERR"))
  {
    bayesline->priors->SAmin = 1.0e-18;
    bayesline->priors->SAmax = 1.0e0;
    bayesline->priors->LQmin = 1.0e2;
    bayesline->priors->LQmax = 1.0e8;
    bayesline->priors->LAmin = 1.0e-14;
    bayesline->priors->LAmax = 1.0e0;
  }

  //aLIGO Design sensitivity
  else if (!strcmp(channel,"LALAdLIGO"))
  {
    bayesline->priors->SAmin = 1.0e-50;
    bayesline->priors->SAmax = 1.0e-43;
    bayesline->priors->LQmin = 1.0e2;
    bayesline->priors->LQmax = 1.0e8;
    bayesline->priors->LAmin = 1.0e-47;
    bayesline->priors->LAmax = 1.0e-43;
  }

  //Use O1 strain as default
  else
  {
    bayesline->priors->SAmin = 1.0e-51*Tobs/4.0;
    bayesline->priors->SAmax = 5.0e-34*Tobs/4.0;
    bayesline->priors->LQmin = 1.0e1;
    bayesline->priors->LQmax = 1.0e9;
    bayesline->priors->LAmin = 1.0e-45;
    bayesline->priors->LAmax = 1.0e-18;
  }

}

int checkrange(double *p, double **r, int NW)
{
   int k;

  if(p[4] < 0.0)        p[4] += LAL_TWOPI;
  if(p[4] >= LAL_TWOPI) p[4] -= LAL_TWOPI;

   for(k=0; k<NW; k++) if(p[k] < r[k][0] || p[k] > r[k][1])
   {
      //printf("parameter %i out of range (%g):  [%g,%g]\n",k,p[k],r[k][0],r[k][1]);
      return 1;
   }
    

    /*
    //keep wavelet from going past Nyquist
    double f = p[1];
    double Q = p[2];
    double fNyq = r[1][1];
    double sq8 = 2.8284;
    double fmax = fNyq/(sq8/Q+1);
    if(f>fmax) return 1;
     */

   return 0;
}

int extrinsic_checkrange(double *p)
{

  int i;
  for(i=0; i<NE; i++)
  {
    if(p[i]!=p[i]) return 1;
  }

  if(p[0] < 0.0)        p[0] += LAL_TWOPI;
  if(p[0] >= LAL_TWOPI) p[0] -= LAL_TWOPI;

  //TODO: sin(DEC) should be periodic
  if(p[1] < -1.0 || p[1] >= 1.0) return 1;


  //psi ranges from [0,pi]
  while(p[2]<0.0 || p[2] >= LAL_PI)
  {
    if(p[2]<0.0) p[2] += LAL_PI;
    else         p[2] -= LAL_PI;

    
  }

  //TODO: ellipticity should be periodic?
  if(p[3] < -0.99 || p[3] >= 0.99) return 1;

  //TODO: phi from [0,2pi], or pi?
  while(p[4]<0.0 || p[4] >= LAL_TWOPI)
  {
    if(p[4] < 0.0)       p[4] += LAL_TWOPI;
    if(p[4] >=LAL_TWOPI) p[4] -= LAL_TWOPI;
  }

  //Each wavelet's amplitude is checked against intrinsic prior during extrinsic updates
  if(p[5] < 0) return 1;

  return 0;
}

static double proximity_prior_density(double t, double f, double **params, int *larray, double *norm, int cnt, int exc, double TFV)
{
   int jj, kk, cx;
   double tau;
   double sigwt, signt, sigwf, signf;
   double sigwt2, signt2, sigwf2, signf2;
   double df, dt, df2, dt2;
   double pden, uf;

   // To maintain the same contrast between regions away from the cluster and in the cluster, the
   // uniform fraction has to go down as we shrink the TF volume. Denisty ratio is TFV*alpha*(1-uf)/uf

   uf = 1.0/(3776.0/TFV+1.0);

   // if no other active wavelets, then prior is uniform in TF volume
   if(cnt < 2)
   {
      pden = 1.0/TFV;
   }
   else
   {

      pden = 0.0;
      cx = 0;

      // loop over the the existing wavelets
      for(jj=1; jj<= cnt; jj++)
      {

         if(jj != exc)  // the prior does not include the density from the wavelet itself
         {

            cx++;

            kk = larray[jj];

            df = fabs(f-params[kk][1]);
            dt = fabs(t-params[kk][0]);

            tau = params[kk][2]/(LAL_TWOPI*params[kk][1]);

            sigwt = SPRD*tau;
            sigwf = SPRD/(LAL_PI*tau);


            if(dt < 6.0*sigwt && df < 6.0*sigwf)   // don't bother adding in contribution from wavelets that are too far away
            {

               signt = HLW*tau;
               sigwt2 = sigwt*sigwt;
               signt2 = signt*signt;

               signf = HLW/(LAL_PI*tau);
               sigwf2 = sigwf*sigwf;
               signf2 = signf*signf;

               df2 = df*df;
               dt2 = dt*dt;

               pden += norm[kk]*(exp(-df2/sigwf2-dt2/sigwt2) -exp(-df2/signf2-dt2/signt2));
            }

         }

      }

      pden /= (double)(cx);

      pden *= (1.0-uf);

      pden += uf/TFV;   // the uniform fraction

   }

   return pden;
}

void proximity_normalization(double **params, double **range, double *norm, int *larray, int cnt)
{
   int i, j;
   double tau;
   double tmin, tmax, sigma_t_thin, sigma_t_fat;
   double fmin, fmax, sigma_f_thin, sigma_f_fat;
    double QV;

    QV = range[2][1]-range[2][0];
   for(i=1; i<= cnt; i++)
   {

      j = larray[i];

      tau = params[j][2]/(LAL_TWOPI*params[j][1]);

      sigma_t_fat  = SPRD*tau;
      sigma_t_thin = HLW*tau;

      sigma_f_fat  = SPRD/LAL_PI/tau;
      sigma_f_thin = HLW/LAL_PI/tau;

      tmin = (range[0][0]-params[j][0]);
      tmax = (range[0][1]-params[j][0]);
      fmin = (range[1][0]-params[j][1]);
      fmax = (range[1][1]-params[j][1]);

      norm[j] = 1./(gaussian_norm(tmin,tmax,sigma_t_fat)*gaussian_norm(fmin,fmax,sigma_f_fat) - gaussian_norm(tmin,tmax,sigma_t_thin)*gaussian_norm(fmin,fmax,sigma_f_thin));
       norm[j] /= QV;
   }
   
}



double wavelet_proximity_prior(struct Wavelet *wave, struct Prior *prior)
{
   int j;
   double logp = 0.0;

   // unpack wave structure
   int cnt = wave->size;
   int *larray  = wave->index;
   double **params = wave->intParams;
   double TFV = prior->TFV;

   double *norm = malloc((prior->smax+1)*sizeof(double));
   
   
   //get normalization for prior
   proximity_normalization(params, prior->range, norm, larray, cnt);
   
   for(j=1; j<= cnt; j++) logp += log(proximity_prior_density(params[larray[j]][0], params[larray[j]][1], params, larray, norm, cnt, j, TFV));
   
   //correct for bias caused by clustering prior
   logp -= prior->bias[cnt];
   
   free(norm);
   
   return logp;
}

double glitch_background_prior(struct Prior *prior, double *params)
{
    double f = params[1];
    double Q = params[2];
    int i,j;

    i = (int)(f/prior->bkg_df);
    j = (int)(Q/prior->bkg_dQ);
    
    return(prior->bkg_dist[i][j]);

}

double glitch_amplitude_prior(double *params, double *Snf, double Tobs, double SNRpeak)
{
   double SNR, alpha;
   double dfac, dfac3;

   //SNR defined with Sn(f) but Snf array holdes <n_i^2>
   SNR = SineGaussianSNR(params, Snf, Tobs);

   dfac = 1.+SNR/(2.*SNRpeak);
   dfac3 = dfac*dfac*dfac;


   alpha = log((SNR)/(2.*SNRpeak*SNRpeak*dfac3)) + log(SNR/params[3]);

   return(alpha);

}


double signal_amplitude_prior(double *params, double *Snf, double Tobs, double SNRpeak)
{
   double SNR, alpha;
   double dfac, dfac5;

   // x/a^2 exp(-x/a) prior on SNR. Peaks at x = a. Good choice is a=5

   //SNR defined with Sn(f) but Snf array holdes <n_i^2>
   SNR = SineGaussianSNR(params, Snf, Tobs);

   dfac = 1.+SNR/(4.*SNRpeak);
   dfac5 = dfac*dfac*dfac*dfac*dfac;


   alpha = log((3.*SNR)/(4.*SNRpeak*SNRpeak*dfac5)) + log(SNR/params[3]);
   
   return(alpha);
   
}

//double uniform_amplitude_prior(double *params, double *Snf, double Sa, double Tobs, double **range)
//{
//  int i;
//  double SNR;
//
//  Sa = 1.0;
//
//  i = (int)(params[1]*Tobs);
//
//  double alpha;
//  double SNRmin = range[3][0];
//  double SNRmax = range[3][1];
//
//  //SNR defined with Sn(f) but Snf array holdes <n_i^2>
//  SNR = SineGaussianSNR(params, Snf, Tobs);
//
//  if(SNR>SNRmin && SNR<=SNRmax)
//  {
//    //SNR defined with Sn(f) but Snf array holdes <n_i^2>
//    alpha = -log(SNRmax-SNRmin) + log(SNR/params[3]);
//  }
//  
//}


