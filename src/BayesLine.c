/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

/*********************************************************************************/
/*                                                                               */
/*     BayesLine fits the LIGO/Virgo power spectra using a model made up         */
/*     of N Lorentzian lines (described by central frequency f, quality          */
/*     factor Q and amplitude A) and cubic spline with M control points.         */
/*     The number of terms in each model, N, M, are free to vary via RJMCMC      */
/*     updates. The code initializes the models in a non-Markovian fashion,      */
/*     then refines the models with a full Markovian RJMCMC subroutine. This     */
/*     subroutine (LorentzMCMC) can be called by other codes to update the       */
/*     spectral fit to the residuals (data - signal model). Doing this is        */
/*     highly recommended, as it ensures that the spectral model is not          */
/*     eating any of the gravitational wave signal. Since the code is            */
/*     transdimensional (and very free in its movement between dimensions)       */
/*     it will not "over-fit" the spectral model.                                */
/*                                                                               */
/*********************************************************************************/


#include "BayesLine.h"

/*
static void system_pause()
{
  printf("Press Any Key to Continue\n");
  getchar();
}
*/

double sample(double *fprop, double pmax, dataParams *data, gsl_rng *r)
{
  int i;
  double f, alpha;

  int ssize    = data->ncut;
  double flow  = data->flow;
  double fhigh = data->fhigh;
  double Tobs  = data->Tobs;

  do
  {
    f = flow+gsl_rng_uniform(r)*(fhigh-flow);
    i = (int)(floor((f-flow)*Tobs));
    if(i < 0) i=0;
    if(i > ssize-1) i=ssize-1;
    alpha = pmax*gsl_rng_uniform(r);
  } while(alpha > fprop[i]);

  return(f);
}


double lprop(double f, double *fprop, dataParams *data)
{
  int i;

  int ssize    = data->ncut;
  double flow  = data->flow;
  double Tobs  = data->Tobs;

  i = (int)((f-flow)*Tobs);
  if(i < 0) i=0;
  else if(i > ssize-1) i=ssize-1;

  return(log(fprop[i]));
}

double loglike_fit_spline(double *respow, double *Snf, int ncut)
{
  double lgl, x;
  int i;

  lgl = 0.0;
  for(i=0; i< ncut; i++)
  {
    x = (respow[i]-Snf[i])*(respow[i]-Snf[i])/0.1;
    lgl -= (x);
  }

  return(lgl);
}

//static double logprior(double *invsigma, double *mean, double *Snf, int ilow, int ihigh)
//static double logprior(double *sigma, double *mean, double *Snf, int ilow, int ihigh)
static double logprior(double *lower, double *upper, double *Snf, int ilow, int ihigh)
{
  //double x;
  double lgp,dS;
  int i;

  //leaving off normalizations since they cancel in Hastings ratio
  lgp = 0;
  for(i=ilow; i<ihigh; i++)
  {
    if(Snf[i]>upper[i])
    {
      dS = log(Snf[i]) - log(upper[i]);
      lgp -= 0.5*dS*dS;
    }
    if(Snf[i]<lower[i])
    {
      dS = log(Snf[i]) - log(lower[i]);
      lgp -= 0.5*dS*dS;
    }
  }
  return (lgp);
}

/*
static double logprior_gaussian_model(double *mean, double *sigma, double *Snf, double *spline_f, int spline_n, double *lines_f, int lines_n, dataParams *data)
{
  double x,f;
  double lgp;
  int i,n;

  double flow  = data->flow;
  double Tobs  = data->Tobs;

  lgp = 0.0;

  //Lorentzian model
  for(n=0; n<lines_n; n++)
  {
    f = lines_f[n];
    i = (int)((f-flow)*Tobs);
    x = (mean[i] - Snf[i])/sigma[i];
    lgp -= x*x;


//    printf("    line %i:  f=%g, PSD=%g, , P=%g, sigma=%g, logP=%g\n",n,f,Snf[i],mean[i],fabs(x),lgp);
  }

  //Spline model
  for(n=0; n<spline_n; n++)
  {
    f = spline_f[n];
    i = (int)((f-flow)*Tobs);
    x = (mean[i] - Snf[i])/sigma[i];
    lgp -= x*x;

//    printf("    spline %i:  f=%g, PSD=%g, , P=%g, sigma=%g, logP=%g\n",n,f,Snf[i],mean[i],fabs(x),lgp);
  }

//  system_pause();

  return (0.5*lgp);
}
*/

static double logprior_gaussian(double *mean, double *sigma, double *Snf, int ilow, int ihigh)
{
  double x;
  double lgp;
  int i;

  //leaving off normalizations since they cancel in Hastings ratio
  lgp = 0;
  
  for(i=ilow; i<ihigh; i++)
  {
     x = (mean[i] - Snf[i])/sigma[i];
     lgp -= x*x;
  }
  
  return (0.0*lgp);
}

static double loglike(double *respow, double *Snf, int ncut)
{
  double lgl, x;
  int i;

  // leavimng out the log(2Pi) terms since they cancel in Hastings ratio
  lgl = 0.0;
  for(i=0; i< ncut; i++)
  {
    x = respow[i]/Snf[i];
    lgl -= (x+log(Snf[i]));
  }

  return(lgl);
}

void spectrum_spline(double *Sn, double *Sbase, double *sfreq, dataParams *data, lorentzianParams *restrict lines, splineParams *restrict spline)
{
  int i, j, k, n;
  int istart, istop;

  int nspline     = spline->n;
  double *spoints = spline->points;
  double *sdata   = spline->data;

  double *x,*Stemp;

  n = data->ncut;

  x     = malloc((size_t)(sizeof(double)*(n)));
  Stemp = malloc((size_t)(sizeof(double)*(n)));

  //Interpolate {spoints,sdata} ==> {sfreq,x}
  CubicSplineGSL(nspline,spoints,sdata,n,sfreq,x);

  for(i=0; i< n; i++)
	{
    Sbase[i] = exp(x[i]);          // spline base model
    Sn[i] = 0.0;
  }

  for(k=0; k<lines->n; k++)
  {
    j = lines->larray[k];
    for(i=0; i<n; i++) Stemp[i]=Sn[i];
    full_spectrum_add_or_subtract(Sn, Stemp, Sbase, sfreq, data, lines, j, &istart, &istop, 1);
  }

  for(i=0; i< n; i++)
  {
    Sn[i] += Sbase[i];
  }

  free(x);
  free(Stemp);
}

double loglike_pm(double *respow, double *Sn, double *Snx, int ilow, int ihigh)
{
  double lgl, x;
  int i;

  // leavimng out the log(2Pi) terms since they cancel in Hastings ratio
  lgl = 0.0;
  for(i=ilow; i< ihigh; i++)
  {
    x = respow[i]/Sn[i]-respow[i]/Snx[i];
    lgl -= (x+log(Sn[i]/Snx[i]));
  }

  return(lgl);
}



double loglike_single(double *respow, double *Sn, double *Snx, int ilowx, int ihighx, int ilowy, int ihighy)
{
  double lgl, x;
  int i;
  int ilow, ihigh;
  int imid1, imid2;

  ilow = ilowx;
  if(ilowy < ilow) ilow = ilowy;

  if(ilow == ilowx)
  {
    if(ihighx <= ilowy)  // separate regions
    {
      imid1 = ihighx;
      imid2 = ilowy;
      ihigh = ihighy;
    }

    if(ihighx > ilowy) // overlapping regions
    {
      if(ihighx < ihighy)
      {
        imid1 = ihighx;
        imid2 = ihighx;
        ihigh = ihighy;
      }
      else
      {
        imid1 = ilowy;
        imid2 = ilowy;
        ihigh = ihighx;
      }
    }
  }

  if(ilow == ilowy)
  {
    if(ihighy <= ilowx)  // separate regions
    {
      imid1 = ihighy;
      imid2 = ilowx;
      ihigh = ihighx;
    }

    if(ihighy > ilowx) // overlapping regions
    {
      if(ihighy < ihighx)
      {
        imid1 = ihighy;
        imid2 = ihighy;
        ihigh = ihighx;
      }
      else
      {
        imid1 = ilowx;
        imid2 = ilowx;
        ihigh = ihighy;
      }
    }
  }

  // leavimng out the log(2Pi) terms since they cancel in Hastings ratio
  lgl = 0.0;
  for(i=ilow; i< imid1; i++)
  {
    x = respow[i]/Sn[i]-respow[i]/Snx[i];
    lgl -= (x+log(Sn[i]/Snx[i]));
  }
  for(i=imid2; i< ihigh; i++)
  {
    x = respow[i]/Sn[i]-respow[i]/Snx[i];
    lgl -= (x+log(Sn[i]/Snx[i]));
  }

  return(lgl);
}

void full_spectrum_add_or_subtract(double *Snew, double *Sold, double *Sbase, double *sfreq, dataParams *restrict data, lorentzianParams *restrict lines, int ii, int *ilow, int *ihigh, int flag)
{
  int i;
  double dS,f2,f4;
  double deltf;
  double fsq, x, z, deltafmax, spread;
  double amplitude;
  int istart, istop, imid, idelt;

  double A = lines->A[ii];
  double Q = lines->Q[ii];
  double f = lines->f[ii];
  double Q2 = Q*Q;
  
  int    ncut = data->ncut;
  double Tobs = data->Tobs;
  double flow = data->flow;

  // copy over current model
  memcpy(Snew, Sold, ncut*sizeof(double));

  // here we figure out how many frequency bins are needed for the line profile
  imid = (int)((f-flow)*Tobs);
  spread = (1.0e-2*Q);

  if(spread < 50.0) spread = 50.0;  // maximum half-width is f_resonance/20
  deltafmax = f/spread;
  deltf = 8.0*deltafmax;
  idelt = (int)(deltf*Tobs)+1;
  if(A < 10.0*Sbase[imid]) idelt = (int)(20.0*f*Tobs/Q)+1;

  istart = imid-idelt;
  istop = imid+idelt;
  if(istart < 0) istart = 0;
  if(istop > ncut) istop = ncut;

  *ilow  = istart;
  *ihigh = istop;


  // add or remove the old line
  f2=f*f;
  f4=f2*f2;
  amplitude = A*f4;///(f2*Q*Q);
  for(i=istart; i<istop; i++)
  {
    fsq = sfreq[i]*sfreq[i];
    x = fabs(f-sfreq[i]);
    z = 1.0;
    if(x > deltafmax) z = exp(-(x-deltafmax)/deltafmax);
    //dS = z*A*f4/(f2*fsq+Q*Q*(fsq-f2)*(fsq-f2));
    if(i==0)
      dS = 0.0;
    else
    {
      //dS = z*amplitude/(fsq*(fsq-f2)*(fsq-f2));
      dS = z*amplitude/( f2*fsq + Q2*(fsq-f2)*(fsq-f2) );
    }
    switch(flag)
    {
      case 1: //add new line
        Snew[i] += dS;
        break;
      case -1: //remove line
        Snew[i] -= dS;
        break;
      default:
        break;
    }
  }
}

void full_spectrum_single(double *Sn, double *Snx, double *Sbasex, double *sfreq, dataParams *data, lorentzianParams *line_x, lorentzianParams *line_y, int ii,
                          int *ilowx, int *ihighx, int *ilowy, int *ihighy)
{

  double *Stemp = malloc((size_t)(sizeof(double)*(data->ncut)));

  full_spectrum_add_or_subtract(Stemp, Snx, Sbasex, sfreq, data, line_x, ii, ilowx, ihighx,-1);
  full_spectrum_add_or_subtract(Sn, Stemp,  Sbasex, sfreq, data, line_y, ii, ilowy, ihighy, 1);

  free(Stemp);
}



void full_spectrum_spline(double *Sline, double *Sbase, double *sfreq, dataParams *restrict data, lorentzianParams *restrict lines)
{
  int i, j, k;
  int istart, istop;

  double *Stemp = malloc((size_t)(sizeof(double)*(data->ncut)));

  for(i=0; i<data->ncut; i++) Sline[i] = 0.0;
  for(k=0; k<lines->n; k++)
  {
    j = lines->larray[k];
    memcpy(Stemp,Sline,data->ncut*sizeof(double));
    full_spectrum_add_or_subtract(Sline, Stemp, Sbase, sfreq, data, lines, j, &istart, &istop, 1);
  }

  free(Stemp);
}

void CubicSplineGSL(int N, double *x, double *y, int Nint, double *xint, double *yint)
{
  int n;

  /* set up GSL cubic spline */
  gsl_spline       *cspline = gsl_spline_alloc(gsl_interp_cspline, N);
  gsl_interp_accel *acc    = gsl_interp_accel_alloc();

  /* get derivatives */
  gsl_spline_init(cspline,x,y,N);

  /* interpolate */
  for(n=0; n<Nint; n++)
  {
    /*
     GSL cubic spline throws errors if
     interpolated points are at end of
     spline control points
     */
    if     (xint[n]<x[0])
      yint[n] = y[0];
    else if(xint[n]>x[N-1])
      yint[n] = y[N-1];
    else
      yint[n]=gsl_spline_eval(cspline,xint[n],acc);
  }

  gsl_spline_free (cspline);
  gsl_interp_accel_free (acc);

}

void create_dataParams(dataParams *data, double *f, int n,int max_lines)
{

  // length of segment in seconds, this should be read in from the frame file
  data->Tobs = rint(1.0/(f[1]-f[0]));

  //minimum frequency
  data->fmin = f[0];

  //maximum frequency
  data->fmax = f[n-1];

  // frequency resolution
  data->df = 1.0/data->Tobs;

  // sample cadence in Hz, this should be read in from the frame file
  data->cadence = pow(2.0,rint(log((double)(n))/log(2.0))+1.0)/data->Tobs;

  // Nyquist frequency
  data->fny = 2.0/data->cadence;

  // size of segments in Hz
  // If frequency snippets are too large need longer initial runs to get convergence
  data->fstep = 1./data->Tobs;//data->fmin;//16;//((data->fmax-data->fmin)/128);//256./data->Tobs;//30;//FSTEP;//9.0;//30.0;

  // This sets the maximum number of Lorentzian lines.
  data->tmax = max_lines;

  // approximate number of segments
  data->sgmts = (int)((f[n-1]-f[0])/data->fstep)+2;

  //minimum Fourier bin
  data->nmin = (int)(f[0]*data->Tobs);

  // the stencil separation in Hz for the spline model. Going below 2 Hz is dangerous - will fight with line model
  data->fgrid = 4.0;//data->fstep/2.;//15.0;//FSTEP;///4;//4.0;//FSTEP;
}

void create_lorentzianParams(lorentzianParams *lines, int size)
{
  lines->n    = 0;
  lines->size = size;

  lines->larray = malloc((size_t)(sizeof(int)*size));

  lines->f = malloc((size_t)(sizeof(double)*size));
  lines->Q = malloc((size_t)(sizeof(double)*size));
  lines->A = malloc((size_t)(sizeof(double)*size));
}

void copy_lorentzianParams(lorentzianParams *origin, lorentzianParams *copy)
{
  copy->n    = origin->n;
  copy->size = origin->size;

  int n;
  for(n=0; n<origin->n; n++)
  {
    copy->larray[n] = origin->larray[n];

    copy->f[n] = origin->f[n];
    copy->Q[n] = origin->Q[n];
    copy->A[n] = origin->A[n];
  }
}

void destroy_lorentzianParams(lorentzianParams *lines)
{
  free(lines->larray);
  free(lines->f);
  free(lines->Q);
  free(lines->A);
  free(lines);
}

void create_splineParams(splineParams *spline, int size)
{
  spline->n = size;

  spline->data   = malloc((size_t)(sizeof(double)*size));
  spline->points = malloc((size_t)(sizeof(double)*size));
}

void copy_splineParams(splineParams *origin, splineParams *copy)
{
  int n;
  copy->n = origin->n;

  for(n=0; n<origin->n; n++)
  {
    copy->data[n]   = origin->data[n];
    copy->points[n] = origin->points[n];
  }
}

void destroy_splineParams(splineParams *spline)
{
  free(spline->data);
  free(spline->points);
  free(spline);
}

void BayesLineLorentzSplineMCMC(struct BayesLineParams *bayesline, double heat, int steps, int focus, int priorFlag, double *dan)
{
  int nsy;
  double logLx, logLy=0.0, logH;
  int ilowx, ihighx, ilowy, ihighy;
  int i, j, k=0, ki=0, ii=0, jj=0, mc;
  int check=0;
  double alpha;
  double lSAmax, lSAmin;
  double lQmin, lQmax;
  double lAmin, lAmax;
  int ac0, ac1, ac2;
  int cc0, cc1, cc2;
  double *Sn, *Sbase, *Sbasex, *Sline, *Snx;
  double *xint;
  double e1, e2, e3, e4;
  double x2, x3, x4;
  double s2, s3, s4;
  int typ=0;
  double xsm, pmax, fcl, fch, dff;
  double baseav;
  double logpx=0.0, logpy=0.0, x, y, z, beta;
  double logPsy=0.0,logPsx=0.0;
  double Ac;
  double *fprop;
  double *sdatay;
  double *spointsy;
  int *foc;

  /* Make local pointers to BayesLineParams structure members */
  dataParams *data           = bayesline->data;
  lorentzianParams *lines_x  = bayesline->lines_x;
  splineParams *spline       = bayesline->spline;
  splineParams *spline_x     = bayesline->spline_x;
  BayesLinePriors *priors    = bayesline->priors;
  double *Snf                = bayesline->Snf;
  double *freq               = bayesline->sfreq;
  double *power              = bayesline->spow;
  gsl_rng *r                 = bayesline->r;

  int ncut = data->ncut;
  int tmax = data->tmax;

  double flow  = data->flow;
  double fhigh = data->fhigh;

  int nspline   = spline->n;

  double *sdatax = spline_x->data;

  double *spoints  = spline->points;
  double *spointsx = spline_x->points;

  Snx    = malloc((size_t)(sizeof(double)*(ncut)));
  Sn     = malloc((size_t)(sizeof(double)*(ncut)));
  Sbasex = malloc((size_t)(sizeof(double)*(ncut)));
  Sbase  = malloc((size_t)(sizeof(double)*(ncut)));
  Sline  = malloc((size_t)(sizeof(double)*(ncut)));

  sdatay   = malloc((size_t)(sizeof(double)*(nspline)));
  spointsy = malloc((size_t)(sizeof(double)*(nspline)));

  foc     = malloc((size_t)(sizeof(int)*(tmax)));

  // This keeps track of whos who in the Lorentzian model
  // Necessary complication when using delta likelihood
  // calculations that only update a single line
  lorentzianParams *lines_y = malloc(sizeof(lorentzianParams));
  create_lorentzianParams(lines_y,tmax);

  fprop = malloc((size_t)(sizeof(double)*(ncut)));
  xint  = malloc((size_t)(sizeof(double)*(ncut)));

  // maxima and minima for the noise spectal slopes and amplitudes
  // uniform priors in slope and log amplitude
  lQmin = log(priors->LQmin);
  lQmax = log(priors->LQmax);
  lAmin = log(priors->LAmin);
  lAmax = log(priors->LAmax);
  lSAmin = log(priors->SAmin);
  lSAmax = log(priors->SAmax);

  dff = 0.01;  // half-width of frequency focus region (used if focus == 1)

  // this is the fractional error estimate on the noise level
  s2 = 0.01;
  s3 = 0.5;
  s4 = 0.5;

  for(i=0; i<lines_x->n; i++)
  {
    lines_x->larray[i] = i;
    lines_y->larray[i] = i;
  }

//  FILE *temp=fopen("start_psd.dat","w");
//  for(i=0; i<ncut; i++) fprintf(temp,"%i %lg %lg %lg\n",i,Snf[i],priors->lower[i],priors->upper[i]);
//  fclose(temp);

  baseav = 0.0;

  //Interpolate {spointsx,sdatax} ==> {freq,xint}
  CubicSplineGSL(spline_x->n,spointsx,sdatax,ncut,freq,xint);

  for(i=0; i<ncut; i++)
  {
    Sbase[i] = exp(xint[i]);
    Sbasex[i] = Sbase[i];
    baseav += xint[i];
  }

  baseav /= (double)(ncut);

  full_spectrum_spline(Sline, Sbase, freq, data, lines_x);
  for(i=0; i< ncut; i++) Sn[i] = Sbase[i]+Sline[i];

  for(i=0; i<ncut; i++) Snx[i] = Sn[i];

  if(!bayesline->constantLogLFlag)
    logLx = loglike(power, Sn, ncut);
  else
    logLx = 1.0;

  if(priorFlag==1)
  {
    logPsx = logprior(priors->lower, priors->upper, Snx, 0, ncut);
  }
  if(priorFlag==2)
  {
    logPsx = logprior_gaussian(priors->mean, priors->sigma, Snx, 0, ncut);
  }

  /*
  if(logPsx<0)
  {
    printf("how did PSD come in outside of the prior?\n");
    printf("%lg\n",logPsx);
    FILE *temp=fopen("failed_psd.dat","w");
    for(i=0; i<ncut; i++) fprintf(temp,"%i %lg %lg %lg\n",i,Snx[i],priors->lower[i],priors->upper[i]);
    
    for(i=0; i<ncut; i++)
      {
	if(Snx[i]>priors->upper[i] || Snx[i]<priors->lower[i])
	  {
	    
	    printf("The PSD is outside the prior. Offending point: %i %lg %lg %lg\n",i,Snx[i],priors->lower[i],priors->upper[i]);                                                                      
	  }
      }


    print_line_model(stdout, bayesline);

    exit(1);
  }
   */

  // set up proposal for frequency jumps
  xsm =0.0;
  pmax = -1.0;
  for(i=0; i< ncut; i++)
  {
    x = power[i]/Sbase[i];
    if(x < 10.0) x = 1.0;
    if(x >= 10.0) x = 100.0;
    if(x > pmax)
    {
      pmax = x;
      k = i;
    }
    fprop[i] = x;
    xsm += x;
  }
  
  // define the focus region (only used if focus flag = 1)
  fcl = freq[k]-dff;
  fch = freq[k]+dff;
  Ac = power[k];
  if(fcl < freq[0]) fcl = freq[0];
  if(fch > freq[ncut-1]) fch = freq[ncut-1];

  ac0 = 0;
  ac1 = 0;
  ac2 = 0;
  cc0 = 1;
  cc1 = 1;
  cc2 = 1;

  for(mc=0; mc < steps; mc++)
  {
    typ=-1;
    check = 0;

    //copy over current state
    lines_y->n = lines_x->n;
    nsy = spline_x->n;
    for(i=0; i< tmax; i++)
    {
      lines_y->larray[i] = lines_x->larray[i];
      lines_y->Q[i] = lines_x->Q[i];
      lines_y->f[i] = lines_x->f[i];
      lines_y->A[i] = lines_x->A[i];
    }
    for(i=0; i<nspline; i++)
    {
      sdatay[i] = sdatax[i];
      spointsy[i] = spointsx[i];
    }

    beta = gsl_rng_uniform(r);

    if(beta > 0.5)  // update the smooth part of the spectrum
    {

      alpha = gsl_rng_uniform(r);

      logpx = logpy = 0.0;

      if(alpha > 0.8)  // try a transdimensional move
      {
        
        //decide between adding or removing spline point
        alpha = gsl_rng_uniform(r);
        if(alpha > 0.5)// || spline_x->n<3)  // try and add a new term
        {
          nsy = spline_x->n+1;
          typ = 5;
        }
        else // try and remove term
        {
          nsy = spline_x->n-1;
          typ = 6;
        }

        //assuming it's an allowed number of spline points, go ahead and propose
        if(nsy > 0 && nsy < nspline)
        {
          //Spline death move
          if(nsy < spline_x->n)
          {

            ki=1+(int)(gsl_rng_uniform(r)*(double)(spline_x->n-2)); // pick a term to try and kill - cant be first or last term
            k = 0;
            for(j=0; j<spline_x->n; j++)
            {
              if(j != ki)
              {
                sdatay[k] = sdatax[j];
                spointsy[k] = spointsx[j];
                k++;
              }
            }

          }//end death moove

          //Spline birth move
          if(nsy > spline_x->n)
          {

            // have to randomly pick a new point that isn't already in use
            do
            {
              ki=1+(int)(gsl_rng_uniform(r)*(double)(nspline-2));  // pick a point to add
              
              ii = 0;
//              printf("   try adding point %i\n",ki);
              for(j=0; j<spline_x->n; j++)
              {
                if(fabs(spointsx[j]-spoints[ki]) < 1.0e-3) ii = 1;  // this means the point is already in use
              }
            } while (ii == 1);
            ii = 0;

            //slot new point into live array
            for(j=0; j<spline_x->n; j++)
            {
              if(spointsx[j] < spoints[ki])
              {
                sdatay[j] = sdatax[j];
                spointsy[j] = spointsx[j];
              }

              if((spointsx[j] > spoints[ki]) && ii == 0)  // found where to slot the new term in
              {
                sdatay[j] = lSAmin +(lSAmax - lSAmin)*gsl_rng_uniform(r);
                spointsy[j] = spoints[ki];
                ii = 1;
              }

              if((spointsx[j] > spoints[ki]) && ii == 1)
              {
                sdatay[j+1] = sdatax[j];
                spointsy[j+1] = spointsx[j];
              }

            }
          }//end birth move
        }
      }
      else  // regular MCMC update
      {
        typ = 4;

        nsy = spline_x->n;

        //pick a term to update
        ii=(int)(gsl_rng_uniform(r)*(double)(spline_x->n));

        // use a variety of jump sizes by using a sum of gaussians of different width
        e1 = 0.0005;
        alpha = gsl_rng_uniform(r);
        if(alpha > 0.8)
        {
          e1 = 0.002;
        }
        else if(alpha > 0.6)
        {
          e1 = 0.005;
        }
        else if(alpha > 0.4)
        {
          e1 = 0.05;
        }

        // propose new value for the selected term
        sdatay[ii] = sdatax[ii]+gsl_ran_gaussian(r, e1);

      }

      if(nsy < 5 || nsy > nspline-1) check = 1;

      for(i=0; i<nsy; i++)
      {
        if(sdatay[i] > lSAmax) check = 1;
        if(sdatay[i] < lSAmin) check = 1;
      }
    }
    else    // update the line model
    {
      alpha = gsl_rng_uniform(r);

      if(alpha > 0.8)  // try a transdimensional move
      {

        alpha = gsl_rng_uniform(r);
        if(alpha < 0.5)  // try and add a new term
        {
          lines_y->n = lines_x->n+1;
          typ = 2;
        }
        else // try and remove term
        {
          lines_y->n = lines_x->n-1;
          typ = 3;
        }

        if(lines_y->n < 0 || lines_y->n > tmax) check = 1;

        if(check == 0)
        {
          if(lines_y->n < lines_x->n)
          {
            i=(int)(gsl_rng_uniform(r)*(double)(lines_x->n)); // pick a term to try and kill
            k = 0;
            for(j=0; j< lines_x->n; j++)
            {
              if(j != i)
              {
                lines_y->larray[k] = lines_x->larray[j];
                jj = lines_x->larray[j];
                lines_y->A[jj] = lines_x->A[jj];
                lines_y->Q[jj] = lines_x->Q[jj];
                lines_y->f[jj] = lines_x->f[jj];
                k++;
              }
              if(j == i) ki = lines_x->larray[j];  // take note of who's demise is being proposed
            }

            logpx = lprop(lines_x->f[ki], fprop, data);
            logpy = -log((double)(ncut));    // corresponds to uniform density - just as likely to kill from anywhere

            y = log(lines_x->A[ki])-baseav;
            if(y < 0.0)
            {
              z = 0.0;
            }
            else
            {
              z = 2.0*kappa_BL*gsl_ran_gaussian_pdf(y, lAwidth);
            }
            logpx += log((1.0-kappa_BL)/(lAmax-lAmin) + z);
            logpy += -log((lAmax-lAmin));
          }

          if(lines_y->n > lines_x->n)
          {
            // copy over current state
            for(j=0; j< lines_x->n; j++)
            {
              k = lines_x->larray[j];
              lines_y->A[k] = lines_x->A[k];
              lines_y->f[k] = lines_x->f[k];
              lines_y->Q[k] = lines_x->Q[k];
              lines_y->larray[j] = lines_x->larray[j];
            }

            // find a label that isn't in use

            i = -1;
            do
            {
              i++;
              k = 0;
              for(j=0; j< lines_x->n; j++)
              {
                if(i==lines_x->larray[j]) k = 1;
              }
            } while(k == 1);

            lines_y->larray[lines_x->n] = i;
            ii=i;

            // draw new term
            lines_y->A[i] = exp(lAmin+(lAmax-lAmin)*gsl_rng_uniform(r));
            lines_y->Q[i] = exp(lQmin+(lQmax-lQmin)*gsl_rng_uniform(r));
            lines_y->f[i] = sample(fprop, pmax, data, r);
            logpy = lprop(lines_y->f[i], fprop, data);
            logpx = -log((double)(ncut));    // corresponds to uniform density - just as likely to kill from anywhere
            alpha = gsl_rng_uniform(r);
            if(alpha < kappa_BL)
            {
              y = fabs(gsl_ran_gaussian(r, lAwidth));
              lines_y->A[i] = exp(baseav+y);
            }
            else
            {
              lines_y->A[i] = exp(lAmin+(lAmax-lAmin)*gsl_rng_uniform(r));
              y = log(lines_y->A[i])-baseav;
            }
            if(y < 0.0)
            {
              z = 0.0;
            }
            else
            {
              z = 2.0*kappa_BL*gsl_ran_gaussian_pdf(y, lAwidth);
            }
            logpy += log((1.0-kappa_BL)/(lAmax-lAmin) + z);
            logpx += -log((lAmax-lAmin));


            if(focus==1) // using focused region (not Markovian - not paying penalty for proposing in such a small region)
            {
              lines_y->f[i] = fcl+(fch-fcl)*gsl_rng_uniform(r);
              lines_y->Q[i] = priors->LQmax/10.0;
              lines_y->A[i] = Ac;

              logpy = 0.0;
              logpx = 0.0;
            }
            if(lines_y->A[i] > priors->LAmax) check = 1;
            if(lines_y->A[i] < priors->LAmin) check = 1;
          }
        }
      }
      else  // regular MCMC update
      {
        lines_y->n = lines_x->n;

        if(lines_y->n > 0)
        {
          typ=1;

          //pick a term to update
          jj=(int)(gsl_rng_uniform(r)*(double)(lines_x->n));
          //find label of who is geting updated
          ii = lines_x->larray[jj];

          // copy over current state
          for(i=0; i< lines_x->n; i++)
          {
            k = lines_x->larray[i];
            lines_y->f[k] = lines_x->f[k];
            lines_y->A[k] = lines_x->A[k];
            lines_y->Q[k] = lines_x->Q[k];
            lines_y->larray[i] = lines_x->larray[i];
          }

          if(focus == 1)
          {
            // find if any lines are in the focus region
            j = 0;
            for(i=0; i< lines_x->n; i++)
            {
              k = lines_x->larray[i];
              if(lines_x->f[k] > fcl && lines_x->f[k] < fch)
              {
                foc[j] = k;
                j++;
              }
            }

            x = 0.0;
            if(j > 0)  // some lines are currently in the focus region
            {
              x = 0.8;
              jj=(int)(gsl_rng_uniform(r)*(double)(j));
              //find label of who is getting updated in the focus region
              ii = foc[jj];
            }

          }
          else
          {
            x = 0.8;
          }

          alpha = gsl_rng_uniform(r);
          if(alpha > x)
          {
            // here we try and move an exisiting line to a totally new location
            if(focus != 1)
            {
              lines_y->f[ii] = sample(fprop, pmax, data, r);
              logpy = lprop(lines_y->f[ii], fprop, data);
              logpx = lprop(lines_x->f[ii], fprop, data);

              lines_y->Q[ii] = exp(lQmin+(lQmax-lQmin)*gsl_rng_uniform(r));
              alpha = gsl_rng_uniform(r);
              if(alpha < kappa_BL)
              {
                y = fabs(gsl_ran_gaussian(r, lAwidth));
                lines_y->A[ii] = exp(baseav+y);
              }
              else
              {
                lines_y->A[ii] = exp(lAmin+(lAmax-lAmin)*gsl_rng_uniform(r));
                y = log(lines_y->A[ii])-baseav;
              }
              if(y < 0.0)
              {
                z = 0.0;
              }
              else
              {
                z = 2.0*kappa_BL*gsl_ran_gaussian_pdf(y, lAwidth);
              }
              logpy += log((1.0-kappa_BL)/(lAmax-lAmin) + z);
              logpx += -log((lAmax-lAmin));


            }
            else  // using focused region (not Markovian - not paying penalty for proposing in such a small region)
            {
              lines_y->f[ii] = fcl+(fch-fcl)*gsl_rng_uniform(r);
              lines_y->Q[ii] = priors->LQmax/10.0;
              lines_y->A[ii] = Ac;

              logpy = 0.0;
              logpx = 0.0;
            }
            typ = 0;
          }
          else
          {
            typ = 1;
            alpha = gsl_rng_uniform(r);

            if     (alpha > 0.9) beta = 1.0e+1;
            else if(alpha > 0.6) beta = 1.0e+0;
            else if(alpha > 0.3) beta = 1.0e-1;
            else                 beta = 1.0e-2;

            e2 = beta*s2;
            e3 = beta*s3;
            e4 = beta*s4;
            x2 = gsl_ran_gaussian(r, e2);
            x4 = gsl_ran_gaussian(r, e4);
            x3 = gsl_ran_gaussian(r, e3);

            lines_y->A[ii] = lines_x->A[ii]*exp(x3);
            lines_y->Q[ii] = lines_x->Q[ii]*exp(x4);
            lines_y->f[ii] = lines_x->f[ii]+x2;

            logpx = logpy = 0.0;
          }

          if(lines_y->A[ii] > priors->LAmax)  check = 1;
          if(lines_y->A[ii] < priors->LAmin)  check = 1;
          if(lines_y->f[ii] < flow)  check = 1;
          if(lines_y->f[ii] > fhigh) check = 1;
          if(lines_y->Q[ii] < priors->LQmin)  check = 1;
          if(lines_y->Q[ii] > priors->LQmax)  check = 1;

        }
        else check=1;
      }


    }

    //If line parameters satisfy priors, continue with MCMC update
    if(check == 0)
    {

      if(typ == 0) cc0++;
      if(typ == 1) cc1++;
      if(typ == 4) cc2++;

      if(typ > 3)  // need to do a full update (slower)
      {
        if(nsy>2)
        {

          //Interpolate {spointsy,sdatay} ==> {freq,xint}
          CubicSplineGSL(nsy,spointsy,sdatay,ncut,freq,xint);

          for(i=0; i<ncut; i++)  Sbase[i] = exp(xint[i]);

          full_spectrum_spline(Sline, Sbase, freq, data, lines_y);
          for(i=0; i< ncut; i++) Sn[i] = Sbase[i]+Sline[i];
          logLy = loglike(power, Sn, ncut);
          if(bayesline->constantLogLFlag) logLy = 1.0;
        }
        else logLy = -1e60;
      }

      if(typ == 1 || typ == 0)  // fixed dimension MCMC of line ii
      {
        full_spectrum_single(Sn, Snx, Sbasex, freq, data, lines_x, lines_y, ii, &ilowx, &ihighx, &ilowy, &ihighy);
        logLy = logLx + loglike_single(power, Sn, Snx, ilowx, ihighx, ilowy, ihighy);
        if(bayesline->constantLogLFlag) logLy = 1.0;
      }

      if(typ == 2)  // add new line with label ii
      {
        full_spectrum_add_or_subtract(Sn, Snx, Sbasex, freq, data, lines_y, ii, &ilowy, &ihighy,  1);
        logLy = logLx + loglike_pm(power, Sn, Snx, ilowy, ihighy);
        if(bayesline->constantLogLFlag) logLy = 1.0;
      }

      if(typ == 3)  // remove line with label ki
      {
        full_spectrum_add_or_subtract(Sn, Snx, Sbasex, freq, data, lines_x, ki, &ilowx, &ihighx, -1);
        logLy = logLx + loglike_pm(power, Sn, Snx, ilowx, ihighx);
        if(bayesline->constantLogLFlag) logLy = 1.0;
      }


      // prior on line number e(-ZETA * n).  (this is a prior, not a proposal, so opposite sign)
      // effectively an SNR cut on lines
      logpy += ZETA*(double)(lines_y->n);
      logpx += ZETA*(double)(lines_x->n);

      //logPsy = logprior(priors->invsigma, priors->mean, Sn, 0, ncut);
      //if(priorFlag)logPsy = logprior(priors->sigma, priors->mean, Sn, 0, ncut);
      if(priorFlag==1)
      {
        logPsy = logprior(priors->lower, priors->upper, Sn, 0, ncut);
        //logPsy = logprior_gaussian_model(priors->mean, priors->sigma, Sn, spointsy, nsy, lines_y->f, lines_y->n, data);
      }
      if(priorFlag==2)
      {
        logPsy = logprior_gaussian(priors->mean, priors->sigma, Sn, 0, ncut);
      }



      logH  = (logLy - logLx)*heat - logpy + logpx;
      if(priorFlag!=0) logH += logPsy - logPsx;

      alpha = log(gsl_rng_uniform(r));

      if(logH > alpha)
      {
        if(typ == 0) ac0++;
        if(typ == 1) ac1++;
        if(typ == 4) ac2++;
        logLx = logLy;
        //if(priorFlag!=0)
        logPsx = logPsy;
        lines_x->n = lines_y->n;
        spline_x->n = nsy;
        for(i=0; i< ncut; i++)
        {
          Snx[i] = Sn[i];
          if(typ > 3) Sbasex[i] = Sbase[i];
        }
        for(i=0; i< tmax; i++)
        {
          lines_x->larray[i] = lines_y->larray[i];
          lines_x->A[i] = lines_y->A[i];
          lines_x->f[i] = lines_y->f[i];
          lines_x->Q[i] = lines_y->Q[i];
        }
        for(i=0; i<spline_x->n; i++)
        {
          sdatax[i] = sdatay[i];
          spointsx[i] = spointsy[i];
        }
      }

    }//end prior check

    //Every 1000 steps update focus region
    if(mc%1000 == 0)
    {
      pmax = -1.0;
      for(i=0; i< ncut; i++)
      {
        x = power[i]/Snx[i];
        if(x > pmax)
        {
          pmax = x;
          k = i;
        }
      }

      // define the focus region (only used if focus flag = 1)
      fcl = freq[k]-dff;
      fch = freq[k]+dff;
      Ac = power[k];
      if(fcl < freq[0]) fcl = freq[0];
      if(fch > freq[ncut-1]) fch = freq[ncut-1];

      //if(focus==1)fprintf(stdout,"Focusing on [%f %f] Hz...\n", fcl, fch);


      // set up proposal for frequency jumps
      xsm =0.0;
      baseav = 0.0;
      for(i=0; i< ncut; i++)
      {
        x = power[i]/Snx[i];
        if(x < 16.0) x = 1.0;
        if(x >= 16.0) x = 100.0;
        fprop[i] = x;
        xsm += x;
        baseav += log(Sbasex[i]);
      }
      baseav /= (double)(ncut);


      pmax = -1.0;
      for(i=0; i< ncut; i++)
      {
        fprop[i] /= xsm;
        if(fprop[i] > pmax) pmax = fprop[i];
      }

    }//end focus update


  }//End MCMC loop
  
  //Interpolate {spointsx,sdatax} ==> {freq,xint}
  CubicSplineGSL(spline_x->n,spointsx,sdatax,ncut,freq,xint);
  for(i=0; i< ncut; i++) Sbase[i] = exp(xint[i]);

  full_spectrum_spline(Sline, Sbase, freq, data, lines_x);
  for(i=0; i< ncut; i++)
  {
    Sn[i] = Sbase[i]+Sline[i];
    bayesline->Sbase[i] = Sbase[i];
    bayesline->Sline[i] = Sline[i];
  }

  // return updated spectral model
  for(i=0; i< ncut; i++) Snf[i] = Snx[i];

  // re-map the array to 0..mx ordering
  for(i=0; i< lines_x->n; i++)
  {
    k = lines_x->larray[i];
    lines_y->f[i] = lines_x->f[k];
    lines_y->A[i] = lines_x->A[k];
    lines_y->Q[i] = lines_x->Q[k];
  }
  
  // return the last value of the chain
  for(i=0; i< lines_x->n; i++)
  {
    lines_x->f[i] = lines_y->f[i];
    lines_x->A[i] = lines_y->A[i];
    lines_x->Q[i] = lines_y->Q[i];
  }
  
  // check for outliers
  pmax = -1.0;
  for(i=0; i< ncut; i++)
  {
    x = power[i]/Snx[i];
    if(x > pmax) pmax = x;
  }
  
  *dan = pmax;

  free(foc);
  free(xint);
  free(fprop);
  free(Snx);
  free(Sn);
  free(Sbase);
  free(Sbasex);
  free(Sline);
  free(sdatay);
  free(spointsy);
  
  destroy_lorentzianParams(lines_y);
  
}

void BayesLineFree(struct BayesLineParams *bptr)
{
  free(bptr->data);

  // storage for line model for a segment. These get updated and passed back from the fitting routine
  destroy_lorentzianParams(bptr->lines_x);

  destroy_splineParams(bptr->spline);
  destroy_splineParams(bptr->spline_x);

  free(bptr->fa);
  free(bptr->Sna);
  free(bptr->freq);
  free(bptr->power);
  free(bptr->Snf);
  free(bptr->Sbase);
  free(bptr->Sline);

  free(bptr->spow);
  free(bptr->sfreq);

  /* set up GSL random number generator */
  gsl_rng_free(bptr->r);

  
  free(bptr);

}

void BayesLineSetup(struct BayesLineParams *bptr, double *freqData, double fmin, double fmax, double deltaT, double Tobs)
{
  int i;
  int n;
  int imin, imax;
  int j;
  double reFreq,imFreq;
  int max_lines=bptr->maxBLLines;

  bptr->data = malloc(sizeof(dataParams));

  bptr->lines_x    = malloc(sizeof(lorentzianParams));

  bptr->spline   = malloc(sizeof(splineParams));
  bptr->spline_x = malloc(sizeof(splineParams));

  bptr->TwoDeltaT = deltaT*2.0;
  
  /* set up GSL random number generator */
  const gsl_rng_type *T = gsl_rng_default;
  bptr->r = gsl_rng_alloc (T);
  gsl_rng_env_setup();

  imin = (int)(fmin*Tobs);
  imax = (int)(fmax*Tobs)+1;
  n    = imax-imin;

  bptr->freq  = malloc((size_t)(sizeof(double)*(n)));
  bptr->power = malloc((size_t)(sizeof(double)*(n)));
  bptr->Snf   = malloc((size_t)(sizeof(double)*(n)));
  bptr->Sbase = malloc((size_t)(sizeof(double)*(n)));
  bptr->Sline = malloc((size_t)(sizeof(double)*(n)));

  for(i=0; i<n; i++)
  {
    j = i+imin;
    bptr->freq[i] = (double)(j)/Tobs;

    reFreq = freqData[2*j];
    imFreq = freqData[2*j+1];

    bptr->power[i] = (reFreq*reFreq+imFreq*imFreq);
  }

  // storage for data meta parameters
  create_dataParams(bptr->data,bptr->freq,n,max_lines);

  // storage for master line model
  create_lorentzianParams(bptr->lines_x,bptr->data->tmax);

  // start with a single line. This number gets updated and passed back
  bptr->lines_x->n = 1;

  //start with ~4 Hz resolution for the spline
  int nspline = (int)(bptr->data->fstep/bptr->data->fgrid)+2;
  int smodn   = nspline*bptr->data->sgmts;

  bptr->fa  = malloc((size_t)(sizeof(double)*(smodn+1)));
  bptr->Sna = malloc((size_t)(sizeof(double)*(smodn+1)));


  //start with ~4 Hz resolution for the spline
  nspline = (int)((bptr->data->fmax-bptr->data->fmin)/bptr->data->fgrid)+2;
  //nspline = (int)(fmax*Tobs);//(int)((bptr->data->fmax-bptr->data->fmin)/bptr->data->fgrid)+2;

  create_splineParams(bptr->spline,nspline);

  create_splineParams(bptr->spline_x,nspline);


  // Re-set dataParams structure to use full dataset
  bptr->data->flow  = bptr->data->fmin;
  bptr->data->fhigh = bptr->data->fmax;
  imax  = (int)(floor(bptr->data->fhigh*bptr->data->Tobs));
  imin  = (int)(floor(bptr->data->flow*bptr->data->Tobs));
  bptr->data->ncut  = imax-imin;

  /*
   The spow and sfreq arrays hold the frequency and power of the full section of data to be whitened
   The model parameters are the number of Lorentzians, nx, and their frequency ff, amplitude AA and
   quality factor QQ, as well as the number of terms in the power law fit to the smooth part of the
   spectrum, segs, and their amplitudes at 100 Hz, SA, and their spectral slopes SP. The maximum number
   of terms in the Lorentzian model is tmax (around 200-300 should cover most spectra), and the
   maximum number of terms in the power law fit is segmax (aound 12-20 should be enough). When called
   from another MCMC code, the LorentzMCMC code needs to be passed the most recent values for the
   noise model. Arrays to hold them have to be declared somewhere in advance. The updated values are
   passed back. The first entry in the call to LoretnzMCMC are the number of iterations. It probably
   makes sense to do 100 or so each time it is called.
   */

  bptr->spow  = malloc((size_t)(sizeof(double)*(bptr->data->ncut)));
  bptr->sfreq = malloc((size_t)(sizeof(double)*(bptr->data->ncut)));

}

void BayesLineInitialize(struct BayesLineParams *bayesline)
{

  int i;
  int imin,imax;
  int j;


  int jj = 0;

  /******************************************************************************/
  /*                                                                            */
  /*  Rapid non-Markovian fit over small bandwidth blocks of data               */
  /*                                                                            */
  /******************************************************************************/

  if(bayesline->lines_x->n < 1 ) bayesline->lines_x->n = 1;

  // Re-set dataParams structure to use full dataset
  bayesline->data->flow  = bayesline->data->fmin;
  imax  = (int)(floor(bayesline->data->fhigh*bayesline->data->Tobs));
  imin  = (int)(floor(bayesline->data->flow*bayesline->data->Tobs));
  bayesline->data->ncut  = imax-imin;

  /******************************************************************************/
  /*                                                                            */
  /*  Full-data spline fit (holding lines fixed from search phase)              */
  /*                                                                            */
  /******************************************************************************/

  int k;
  double mdn;
  double x,z;

  /* Make local pointers to BayesLineParams structure members */
  dataParams *data             = bayesline->data;
  splineParams *spline         = bayesline->spline;
  double *Sna                  = bayesline->Sna;
  double *fa                   = bayesline->fa;


  //initialize spline grid & PSD values
  k=0;
  for(j=0; j<bayesline->spline->n; j++)
  {
    x = data->fmin+(data->fhigh-data->fmin)*(double)(j)/(double)(bayesline->spline->n-1);

    if     (x <= fa[0])    z = Sna[0];
    else if(x >= fa[jj-1]) z = Sna[jj-1];
    else
    {
      i=k-10;
      mdn = 0.0;
      do
      {
        if(i>=0) mdn = fa[i];
        i++;
      } while(x > mdn);

      k = i;
      z = Sna[i];
    }

    spline->points[j] = x;
    spline->data[j]   = z;
  }

  for(j=0; j<bayesline->spline->n; j++)
  {
    bayesline->spline_x->points[j] = bayesline->spline->points[j];
    bayesline->spline_x->data[j]   = bayesline->spline->data[j];
  }

  bayesline->data->flow  = bayesline->data->fmin;

  imin  = (int)(floor(bayesline->data->flow*bayesline->data->Tobs));

  for(i=0; i< bayesline->data->ncut; i++)
  {
    j = i + imin - bayesline->data->nmin;
    bayesline->spow[i] = bayesline->power[j];
    bayesline->sfreq[i] = bayesline->freq[j];
  }

}

void BayesLineRJMCMC(struct BayesLineParams *bayesline, double *freqData, double *psd, double *invpsd, double *splinePSD, int N, int cycle, double beta, int priorFlag)
{
  int i,j;
  int imin,imax;
  double reFreq,imFreq;
  double dan;

  //at frequencies below fmin output SAmax (killing lower frequencies in any future inner products)
  imax = (int)(floor(bayesline->data->fmax * bayesline->data->Tobs));
  imin = (int)(floor(bayesline->data->fmin * bayesline->data->Tobs));

  for(i=0; i< bayesline->data->ncut; i++)
  {
    j = i + imin;

    reFreq = freqData[2*j];
    imFreq = freqData[2*j+1];

    bayesline->spow[i] = (reFreq*reFreq+imFreq*imFreq);
  }

  BayesLineLorentzSplineMCMC(bayesline, beta, cycle, 0, priorFlag, &dan);

  /******************************************************************************/
  /*                                                                            */
  /*  Output PSD in format for BayesWave                                        */
  /*                                                                            */
  /******************************************************************************/

  for(i=0; i<N/2; i++)
  {
    if(i>=imin && i<imax)
    {
      psd[i] = bayesline->Snf[i-imin];
      splinePSD[i] = bayesline->Sbase[i-imin];
    }
    else
    {
      psd[i] = 1.0;
      splinePSD[i] = 1.0;
    }
    invpsd[i] = 1./psd[i];
  }
}


void copy_bayesline_params(struct BayesLineParams *origin, struct BayesLineParams *copy)
{
  copy->data->tmax = origin->data->tmax;

  int i;
  for(i=0; i<origin->data->ncut; i++)
  {
    copy->spow[i]  = origin->spow[i];
    copy->sfreq[i] = origin->sfreq[i];
  }

  int imax = (int)(floor(origin->data->fmax  * origin->data->Tobs));
  int imin = (int)(floor(origin->data->fmin  * origin->data->Tobs));
  for(i=0; i<imax-imin; i++)
  {
    copy->Snf[i]    = origin->Snf[i];
    copy->spow[i]   = origin->spow[i];
    copy->sfreq[i]  = origin->sfreq[i];
    copy->Sbase[i]  = origin->Sbase[i];
    copy->Sline[i]  = origin->Sline[i];
  }

  copy_splineParams(origin->spline, copy->spline);
  copy_splineParams(origin->spline_x,copy->spline_x);
  copy_lorentzianParams(origin->lines_x, copy->lines_x);

}

void print_line_model(FILE *fptr, struct BayesLineParams *bayesline)
{
  int j;
  
  fprintf(fptr,"%i ", bayesline->lines_x->n);
  for(j=0; j< bayesline->lines_x->n; j++) fprintf(fptr,"%lg %lg %lg ", bayesline->lines_x->f[j], bayesline->lines_x->A[j], bayesline->lines_x->Q[j]);
  fprintf(fptr,"\n");

}
void print_spline_model(FILE *fptr, struct BayesLineParams *bayesline)
{
  int j;

  fprintf(fptr,"%i ", bayesline->spline_x->n);
  for(j=0; j<bayesline->spline_x->n; j++) fprintf(fptr,"%.4lf %lg ",bayesline->spline_x->points[j],bayesline->spline_x->data[j]);
  fprintf(fptr,"\n");
}

void parse_line_model(FILE *fptr, struct BayesLineParams *bayesline)
{
  int j;

  fscanf(fptr,"%i",&bayesline->lines_x->n);
  for(j=0; j< bayesline->lines_x->n; j++) fscanf(fptr,"%lg %lg %lg",&bayesline->lines_x->f[j],&bayesline->lines_x->A[j],&bayesline->lines_x->Q[j]);
}
void parse_spline_model(FILE *fptr, struct BayesLineParams *bayesline)
{
  int j;

  fscanf(fptr,"%i",&bayesline->spline_x->n);
  for(j=0; j<bayesline->spline_x->n; j++)fscanf(fptr,"%lg %lg",&bayesline->spline_x->points[j],&bayesline->spline_x->data[j]);
}


/******************************************************************************/
/*                                                                            */
/*  New for O3 BayesLine burn in functions                                    */
/*                                                                            */
/******************************************************************************/

#include <gsl/gsl_statistics.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_sort.h>

#define Qs 8.0          // Q used in the glitch cleaning for spectral estimation
#define fsp 4.0         // spacing of spline points in Hz
#define TPI 6.2831853071795862319959269370884
#define RTPI 1.772453850905516                    // sqrt(Pi)
#define sthresh 10.0
#define warm 6.0
#define LN2 0.6931471805599453                 // ln 2


#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

static double *double_vector(int N)
{
  return malloc( (N+1) * sizeof(double) );
}

static void free_double_vector(double *v)
{
  free(v);
}

static double **double_matrix(int N, int M)
{
  int i;
  double **m = malloc( (N+1) * sizeof(double *));
  
  for(i=0; i<N+1; i++)
  {
    m[i] = malloc( (M+1) * sizeof(double));
  }
  
  return m;
}

static void free_double_matrix(double **m, int N)
{
  int i;
  for(i=0; i<N+1; i++) free_double_vector(m[i]);
  free(m);
}


static void tukey(double *data, double alpha, int N)
{
  int i, imin, imax;
  double filter;
  
  imin = (int)(alpha*(double)(N-1)/2.0);
  imax = (int)((double)(N-1)*(1.0-alpha/2.0));
  
    int Nwin = N-imax;

    for(i=0; i< N; i++)
  {
    filter = 1.0;
    if(i<imin) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-1.0 )));
    if(i>imax) filter = 0.5*(1.0+cos(M_PI*( (double)(i-imax)/(double)(Nwin))));
    data[i] *= filter;
  }
  
}

static void tukey_scale(double *s1, double *s2, double alpha, int N)
{
  int i, imin, imax;
  double x1, x2;
  double filter;
  
  imin = (int)(alpha*(double)(N-1)/2.0);
  imax = (int)((double)(N-1)*(1.0-alpha/2.0));
  
    int Nwin = N-imax;

  x1 = 0.0;
  x2 = 0.0;
  for(i=0; i< N; i++)
  {
    filter = 1.0;
    if(i<imin) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-1.0 )));
    if(i>imax) filter = 0.5*(1.0+cos(M_PI*( (double)(i-imax)/(double)(Nwin))));
    x1 += filter;
    x2 += filter*filter;
  }
  x1 /= (double)(N);
  x2 /= (double)(N);
  
  *s1 = x1;
  *s2 = sqrt(x2);
  
}

static void phase_blind_time_shift(double *corr, double *corrf, double *data1, double *data2, int n)
{
  int nb2, i, l, k;
  
  nb2 = n / 2;
  
  corr[0] = 0.0;
  corrf[0] = 0.0;
  corr[nb2] = 0.0;
  corrf[nb2] = 0.0;
  
  for (i=1; i < nb2; i++)
  {
    l=i;
    k=n-i;
    
    corr[l]  = (data1[l]*data2[l] + data1[k]*data2[k]);
    corr[k]  = (data1[k]*data2[l] - data1[l]*data2[k]);
    corrf[l] = corr[k];
    corrf[k] = -corr[l];
  }
  
  gsl_fft_halfcomplex_radix2_inverse(corr, 1, n);
  gsl_fft_halfcomplex_radix2_inverse(corrf, 1, n);
  
  
}

static void SineGaussianC(double *hs, double *sigpar, double Tobs, int NMAX)
{
  double f0, Q, sf;
  double fmax, fmin, fac;
  double f;
  double tau, dt;
  int imin, imax;
  
  int i;
  
  // Torrence and Compo
  
  f0 = sigpar[1];
  Q = sigpar[2];
  
  tau = Q/(TPI*f0);
  
  dt = Tobs/(double)(NMAX);
  
  fmax = f0 + 3.0/tau;  // no point evaluating waveform past this time (many efolds down)
  fmin = f0 - 3.0/tau;  // no point evaluating waveform before this time (many efolds down)
  
  fac = sqrt(sqrt(2.0)*M_PI*tau/dt);
  
  i = (int)(f0*Tobs);
  imin = (int)(fmin*Tobs);
  imax = (int)(fmax*Tobs);
  if(imax - imin < 10)
  {
    imin = i-5;
    imax = i+5;
  }
  
  if(imin < 0) imin = 0;
  if(imax > NMAX/2) imax = NMAX/2;
  
  hs[0] = 0.0;
  hs[NMAX/2] = 0.0;
  
  for(i = 1; i < NMAX/2; i++)
  {
    hs[i] = 0.0;
    hs[NMAX-i] = 0.0;
    
    if(i > imin && i < imax)
    {
      f = (double)(i)/Tobs;
      sf = fac*exp(-M_PI*M_PI*tau*tau*(f-f0)*(f-f0));
      hs[i] = sf;
      hs[NMAX-i] = 0.0;
    }
    
  }
  
  
}

static double f_nwip(double *a, double *b, int n)
{
  int i, j, k;
  double arg, product;
  double ReA, ReB, ImA, ImB;
  
  arg = 0.0;
  for(i=1; i<n/2; i++)
  {
    j = i;
    k = n-1;
    ReA = a[j]; ImA = a[k];
    ReB = b[j]; ImB = b[k];
    product = ReA*ReB + ImA*ImB;
    arg += product;
  }
  
  return(arg);
  
}

static void TransformC(double *a, double *freqs, double **tf, double **tfR, double **tfI, double Q, double Tobs, int n, int m)
{
  int i, j;
  double f;
  double *AC, *AF;
  double *b;
  double *params;
  double bmag;
  
  // [0] t0 [1] f0 [2] Q [3] Amp [4] phi
  
  params= double_vector(5);
  
  params[0] = 0.0;
  params[2] = Q;
  params[3] = 1.0;
  params[4] = 0.0;

  AC=double_vector(n-1);
  AF=double_vector(n-1);
  
  b = double_vector(n-1);
  
  double fix = sqrt((double)(n/2));
  
  
  for(j = 0; j < m; j++)
  {
    
    f = freqs[j];
    
    params[1] = f;
    
    SineGaussianC(b, params, Tobs, n);
    
    bmag = sqrt(f_nwip(b, b, n)/(double)n);
    
    bmag /= fix;
    
    //printf("%f %f\n", f, bmag);
    
    phase_blind_time_shift(AC, AF, a, b, n);
    
    
    for(i = 0; i < n; i++)
    {
      
      tfR[j][i] = AC[i]/bmag;
      tfI[j][i] = AF[i]/bmag;
      tf[j][i] = tfR[j][i]*tfR[j][i]+tfI[j][i]*tfI[j][i];
    }
    
  }
  
  
  free_double_vector(AC);
  free_double_vector(AF);
  free_double_vector(b);
  free_double_vector(params);
  
}

static double Getscale(double *freqs, double Q, double Tobs, double fmax, int n, int m)
{
  double *data, *intime, *ref, **tfR, **tfI, **tf;
  double f, t0, delt, t, x, dt;
  double scale, sqf;
  int i, j;
  
  data   = double_vector(n-1);
  ref    = double_vector(n-1);
  intime = double_vector(n-1);
  
  tf  = double_matrix(m-1,n-1);
  tfR = double_matrix(m-1,n-1);
  tfI = double_matrix(m-1,n-1);
  
  f = fmax/4.0;
  t0 = Tobs/2.0;
  delt = Tobs/8.0;
  dt = Tobs/(double)(n);
  
  //out = fopen("packet.dat","w");
  for(i=0; i< n; i++)
  {
    t = (double)(i)*dt;
    x = (t-t0)/delt;
    x = x*x/2.0;
    data[i] = cos(TPI*t*f)*exp(-x);
    ref[i] = data[i];
    //fprintf(out,"%e %e\n", t, data[i]);
  }
  // fclose(out);
  
  
  gsl_fft_real_radix2_transform(data, 1, n);

  TransformC(data, freqs, tf, tfR, tfI, Q, Tobs, n, m);
  
  
  for(i = 0; i < n; i++) intime[i] = 0.0;
  
  for(j = 0; j < m; j++)
  {
    
    f = freqs[j];
    
    
    sqf = sqrt(f);
    
    for(i = 0; i < n; i++)
    {
      intime[i] += sqf*tfR[j][i];
    }
    
  }
  
  x = 0.0;
  j = 0;
  // out = fopen("testtime.dat","w");
  for(i=0; i< n; i++)
  {
    // fprintf(out,"%e %e %e\n",times[i], intime[i], ref[i]);
    
    if(fabs(ref[i]) > 0.01)
    {
      j++;
      x += intime[i]/ref[i];
    }
  }
  //fclose(out);
  
  x /= sqrt((double)(2*n));
  
  scale = (double)j/x;
  
  // printf("scaling = %e %e\n", x/(double)j, (double)j/x);
  
  free_double_vector(data);
  free_double_vector(ref);
  free_double_vector(intime);
  free_double_matrix(tf, m-1);
  free_double_matrix(tfR,m-1);
  free_double_matrix(tfI,m-1);
  
  return scale;
  
}

static void spectrum(double *data, double *S, double *Sn, double *Smooth, double df, int N)
{
  double Dfmax, x;
  double Df1, Df2;
  int mw, k, i, j;
  int mm, kk;
  int end1, end2, end3;
  double *chunk;
  
  // log(2) is median/2 of chi-squared with 2 dof
  
  
  for(i=1; i< N/2; i++) S[i] = 2.0*(data[i]*data[i]+data[N-i]*data[N-i]);
  S[0] = S[1];
  
  Dfmax = 8.0; // is the  width of smoothing window in Hz
  
  // Smaller windows used initially where the spectrum is steep
  Df2 = Dfmax/2.0;
  Df1 = Dfmax/4.0;
  
  // defines the ends of the segments where smaller windows are used
  end1 = (int)(32.0/df);
  end2 = 2*end1;
  
  mw = (int)(Dfmax/df)+1;  // size of median window
  //printf("numer of bins in smoothing window %d\n", mw);
  k = (mw+1)/2;
  chunk = double_vector(mw);
  
  end3 = N/2-k;  // end of final chunk
  
  // Fill the array so the ends are not empty - just to be safe
  for(i=0;i< N/2;i++)
  {
    Sn[i] = S[i];
    Smooth[i] = S[i];
  }
  
  
  mw = (int)(Df1/df)+1;  // size of median window
  k = (mw+1)/2;
  
  for(i=4; i< k; i++)
  {
    mm = i/2;
    kk = (mm+1)/2;
    
    for(j=0;j< mm;j++)
    {
      chunk[j] = S[i-kk+j];
    }
    
    gsl_sort(chunk,1,mm);
    Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm)/LN2; // chi-squared with two degrees of freedom
    Smooth[i] = Sn[i];
    
  }
  
  
  i = k;
  do
  {
    for(j=0;j< mw;j++)
    {
      chunk[j] = S[i-k+j];
    }
    
    gsl_sort(chunk,1,mw);
    Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw)/LN2; // chi-squared with two degrees of freedom
    Smooth[i] = Sn[i];
    
    i++;
    
  }while(i < end1);
  
  
  
  mw = (int)(Df2/df)+1;  // size of median window
  k = (mw+1)/2;
  
  
  do
  {
    for(j=0;j< mw;j++)
    {
      chunk[j] = S[i-k+j];
    }
    
    gsl_sort(chunk,1,mw);
    Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw)/LN2; // chi-squared with two degrees of freedom
    Smooth[i] = Sn[i];
    
    i++;
    
  }while(i < end2);
  
  mw = (int)(Dfmax/df)+1;  // size of median window
  k = (mw+1)/2;
  
  do
  {
    for(j=0;j< mw;j++)
    {
      chunk[j] = S[i-k+j];
    }
    
    gsl_sort(chunk,1,mw);
    Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw)/LN2; // chi-squared with two degrees of freedom
    Smooth[i] = Sn[i];
    
    i++;
    
  }while(i < end3);
  
  
  for(i=end3; i< N/2-4; i++)
  {
    mm = (N/2-i)/2;
    kk = (mm+1)/2;
    
    for(j=0;j< mm;j++)
    {
      chunk[j] = S[i-kk+j];
    }
    
    gsl_sort(chunk,1,mm);
    Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm)/LN2; // chi-squared with two degrees of freedom
    Smooth[i] = Sn[i];
    
  }

  for(i=N/2-4; i< N/2; i++) Smooth[i] = Sn[i] = Sn[N/2-4-1];

  
  free_double_vector(chunk);
  
  
  
  
  // zap the lines.
  for(i=1;i< N/2;i++)
  {
    x = S[i]/Sn[i];
    if(x > 10.0)
    {
      Sn[i] = S[i];
    }
  }
  
  
  
}


static void whiten(double *data, double *Sn, int N)
{
  double x;
  int i;
  
  data[0] = 0.0;
  data[N/2] = 0.0;
  
  for(i=1; i< N/2; i++)
  {
    x = 1.0/sqrt(Sn[i]);
    data[i] *= x;
    data[N-i] *= x;
  }
  
}

static void clean(double *D, double *Draw, double *sqf, double *freqs, double *Sn, double *specD, double *sspecD, double df, double Q, double Tobs, double scale, double alpha, int Nf, int N, int imin, int imax, double *SNR)
{
  
  int i, j, k;
  int flag;
  int ii, jj;
  double x;
  double S;

  // allocate some arrays
  double **tfDR, **tfDI;
  double **tfD;
  double **live;
  double **live2;
  
  double *Dtemp;
  
  live= double_matrix(Nf-1,N-1);
  live2= double_matrix(Nf-1,N-1);
  tfDR = double_matrix(Nf-1,N-1);
  tfDI = double_matrix(Nf-1,N-1);
  tfD = double_matrix(Nf-1,N-1);
  
  
  Dtemp = (double*)malloc(sizeof(double)*(N));
  
  for (i = 0; i < N; i++) Dtemp[i] = Draw[i];
  
  // D holds the previously cleaned data
  // Draw holds the raw data
  
  // D is used to compute the spectrum. A copy of Draw is then whitened using this spectrum and glitches are then identified
  // The glitches are then re-colored, subtracted from Draw to become the new D
  
  
  // Tukey window
  tukey(D, alpha, N);
  tukey(Dtemp, alpha, N);
  
  
  // FFT
  gsl_fft_real_radix2_transform(D, 1, N);
  gsl_fft_real_radix2_transform(Dtemp, 1, N);
  
  // Form spectral model for whitening data (lines plus a smooth component)
  spectrum(D, Sn, specD, sspecD, df, N);
  
  // whiten data
  whiten(Dtemp, specD, N);
  
  // Wavelet transform
  TransformC(Dtemp, freqs, tfD, tfDR, tfDI, Q, Tobs, N, Nf);
  
  k = 0;
  //  apply threshold
  for(j = 0; j < Nf; j++)
  {
    for (i = 0; i < N; i++)
    {
      live[j][i] = -1.0;
      if(tfD[j][i] > sthresh) live[j][i] = 1.0;
      if(tfD[j][i] > sthresh) k++;
      live2[j][i] = live[j][i];
    }
  }
  
  
  // dig deeper to extract clustered power
  for(j = 1; j < Nf-1; j++)
  {
    for (i = 1; i < N-1; i++)
    {
      
      flag = 0;
      for(jj = -1; jj <= 1; jj++)
      {
        for(ii = -1; ii <= 1; ii++)
        {
          if(live[j+jj][i+ii] > 0.0) flag = 1;
        }
      }
      if(flag == 1 && tfD[j][i] > warm) live2[j][i] = 1.0;
    }
  }
  
  
  // build the excess power model
  for (i = 0; i < N; i++)
  {
    Dtemp[i] = 0.0;
  }
  
  k = 0;
  for(j = 0; j < Nf; j++)
  {
    for (i = imin; i < imax; i++)
    {
      if(live2[j][i] > 0.0) Dtemp[i] += scale*sqf[j]*tfDR[j][i];
    }
  }
  
  
  /*out = fopen("wglitch.dat", "w");
   for(i=0; i< N; i++)
   {
   fprintf(out,"%d %e\n", i, Dtemp[i]);
   }
   fclose(out);*/
  
  
  // Compute the excess power (relative to the current spectral model
  S = 0.0;
  for (i = imin; i < imax; ++i) S += Dtemp[i]*Dtemp[i];
  S = sqrt(S);
  
  printf("   Excess SNR = %f\n", S);
  
  *SNR = S;
  
  
  //Unwhiten and subtract the excess power so we can compute a better spectral estimate
  // Back to frequency domain
  
  gsl_fft_real_radix2_transform(Dtemp, 1, N);
  
  
  // only use smooth spectrum in the un-whitening
  Dtemp[0] = 0.0;
  for(i=1; i< N/2; i++)
  {
    x = sqrt(sspecD[i]);
    Dtemp[i] *= x;
    Dtemp[N-i] *= x;
  }
  Dtemp[N/2] = 0.0;
  
  gsl_fft_halfcomplex_radix2_inverse(Dtemp, 1, N);
  
  
  x = sqrt((double)(2*N));
  
  for(i=0; i< N; i++)
  {
    D[i] = Draw[i]-Dtemp[i]/x;
  }
  
  /*out = fopen("glitch.dat", "w");
   for(i=0; i< N; i++)
   {
   fprintf(out,"%d %e\n", i, Dtemp[i]/x);
   }
   fclose(out);*/
  
  
  free(Dtemp);
  free_double_matrix(live, Nf-1);
  free_double_matrix(live2,Nf-1);
  free_double_matrix(tfDR, Nf-1);
  free_double_matrix(tfDI, Nf-1);
  free_double_matrix(tfD,  Nf-1);
  
  
  return;
  
  
}
void blstart(double *data, double *residual, int N, double dt, double fmin, int *Nsp, int *Nl, double *dspline, double *pspline, double *linef, double *lineh, double *lineQ, int max_lines, char *ifo)
{
  int i, j, k, Nf,  ii;
  int Nlines;
  int flag;
  int imin, imax;
  double SNR, max;
  double Tobs, f, df, x, dx;
  double fmax, Q, fny, scale;
  double *freqs;
  double *Draw;
  double *D;
  double *Sn;
  double *specD, *sspecD;
  double *sqf;
  int subscale, octaves;
  double SNRold;
  double alpha;
  double t_rise, s1, s2, fac;
  double *linew;
  
  char filename[1024];
  
  FILE *outFile;
  
  Q = Qs;  // Q of transform
  Tobs = (double)(N)*dt;  // duration
  
  // Tukey window parameter. Flat for (1-alpha) of data
  //TODO: NEED TO MAKE THIS A PARAMETER WE CAN CONTROL AT THE COMMAND LINE
  t_rise = 0.4; // Standard LAL setting
  alpha = (2.0*t_rise/Tobs);
  
  df = 1.0/Tobs;  // frequency resolution
  fny = 1.0/(2.0*dt);  // Nyquist
  
  // Set the range of the spectrogram. fmin, fmax must be a power of 2
  fmax = fny;
  
  
  D = (double*)malloc(sizeof(double)* (N));
  Draw = (double*)malloc(sizeof(double)* (N));
  
  linew = (double*)malloc(sizeof(double)*(max_lines));
  
  // Copy data over
  for(i=0; i< N; i++)
  {
    Draw[i] = data[i];
    D[i] = data[i];
  }
  
  
  // Normalization factor
  tukey_scale(&s1, &s2, alpha, N);
  
  
  // Time series data is corrupted by the Tukey window at either end
  // imin and imax define the "safe" region to use
  imin = (int)(2.0*t_rise/dt);
  imax = N-imin;
  
  
  // Prepare to make spectogram
  
  // logarithmic frequency spacing
  subscale = 40;  // number of semi-tones per octave
  octaves = (int)(rint(log(fmax/fmin)/log(2.0))); // number of octaves
  Nf = subscale*octaves+1;
  freqs = (double*)malloc(sizeof(double)* (Nf));   // frequencies used in the analysis
  sqf = (double*)malloc(sizeof(double)* (Nf));
  dx = log(2.0)/(double)(subscale);
  x = log(fmin);
  for(i=0; i< Nf; i++)
  {
    freqs[i] = exp(x);
    sqf[i] = sqrt(freqs[i]);
    x += dx;
  }
  
  //printf("frequency layers = %d\n", Nf);
  
  scale = Getscale(freqs, Q, Tobs, fmax, N, Nf);
  
  
  sspecD = (double*)malloc(sizeof(double)*(N/2));
  specD = (double*)malloc(sizeof(double)*(N/2));
  Sn = (double*)malloc(sizeof(double)*(N/2));
  
  
  
  SNRold = 0.0;
  clean(D, Draw, sqf, freqs, Sn, specD, sspecD, df, Q, Tobs, scale, alpha, Nf, N, imin, imax, &SNR);
  
  
  // if big glitches are present we need to rinse and repeat
  i = 0;
  while(i < 5 && (SNR-SNRold) > 10.0)
  {
    SNRold = SNR;
    clean(D, Draw, sqf, freqs, Sn, specD, sspecD, df, Q, Tobs, scale, alpha, Nf, N, imin, imax, &SNR);
    i++;
  }
  
  
  // re-compute the power spectrum using the cleaned data
  tukey(D, alpha, N);
  
  gsl_fft_real_radix2_transform(D, 1, N);

  // Form spectral model for whitening data (lines plus a smooth component)
  spectrum(D, Sn, specD, sspecD, df, N);
  
  fac = Tobs*Tobs/((double)(N)*(double)(N))/2.;

  //copy output of clean (D) into residual for output
  residual[0] = 0.0;
  residual[1] = 0.0;
  for(i=1; i< N/2; i++)
  {
    residual[2*i]   = D[i]   * sqrt(fac);
    residual[2*i+1] = D[N-i] * sqrt(fac);
  }

  /*
   sprintf(filename, "PSD_%d_%d.dat", (int)(Q), (int)(Tobs));
   outFile = fopen(filename,"w");
   for (i = 1; i < N/2; ++i)
   {
   fprintf(outFile,"%.15e %.15e %.15e %.15e\n", (double)(i)/Tobs, Sn[i]*fac, specD[i]*fac, sspecD[i]*fac);
   }
   fclose(outFile);
   */
  
  // set up the spline model
  int Nspline;
  
  
  Nspline = (int)((fmax-fmin)/fsp)+1;
  // make sure that the last point is at or below fmax
  f = fmin+(double)(Nspline-1)*fsp;
  k = (int)(rint(f*Tobs));
  //if(k > N/2-1) Nspline = Nspline-1;
  
  for (i = 0; i < Nspline; ++i)
  {
    f = fmin+(double)(i)*fsp;
    k = (int)(rint(f*Tobs));
    
    //catch k going out of bounds
    if(k<N/2)
    {
      pspline[i] = (double)(k)/Tobs;
      dspline[i] = log(sspecD[k]*fac);
    }
    else
    {
      pspline[i] = (double)(N/2-1)/Tobs;
      dspline[i] = log(sspecD[N/2-1]*fac);
    }
  }
  
  
  
  double xold, xnext, Abar;
  
  j = 0;
  xold = 1.0;
  flag = 0;
  imin = (int)(fmin*Tobs);
  for (i = imin; i < N/2-1; ++i)
  {
    x = specD[i]/sspecD[i];
    xnext = specD[i+1]/sspecD[i+1];
    // start of a line
    if((x > xold && x > 5.0) && flag == 0)
    {
      k = 1;
      flag = 1;
      max = x;
      ii = i;
    }
    // in a line
    if(x > 5.0 && x > xold && flag ==1)
    {
      k++;
      if(x > max)
      {
        max = x;
        ii = i;
      }
    }
    // have reached the end of a line
    if(flag == 1)
    {
      if(x < 5.0 || (x < xold && xnext > x))
      {
        flag = 0;
        linew[j] = (double)(k)/Tobs;
        linef[j] = (double)(ii)/Tobs;
        lineh[j] = (max-1.0)*sspecD[ii]*fac;
        Abar = 0.5*(specD[ii+1]/sspecD[ii+1]+specD[ii-1]/sspecD[ii-1]);
        lineQ[j] = sqrt((max/Abar-1.0))*linef[j]*Tobs;
        j++;
        //printf("%d %e %e %e\n", j, linef[j], linew[j], lineQ[j]);
      }
    }
    
    
    xold = x;
  }
  
  Nlines = j;
  
  // printf("There are %d lines\n", Nlines);
  
  *Nl = Nlines;
  *Nsp = Nspline;
  
  /* The information needed for the Bayesline algorithm is:
   Nlines  // number of lines
   linef   // line central frequency
   lineh   // line height;
   lineQ   // line Q;
   Nspline  // number of spline points
   pspline  // reference grid of spline control points
   dspline  // values at reference points
   */
  for (i = 1; i < N/2; ++i) Sn[i] = 0.0;
  for (i = imin; i < N/2; ++i)
  {
    f = (double)(i)/Tobs;
    for (j = 0; j < Nlines; ++j)
    {
      x = 0.25*fabs(f - linef[j]);
      if(x < linew[j])
      {
        Sn[i] += lineh[j]*pow(linef[j],4.0)/(pow(f*linef[j],2.0)+lineQ[j]*lineQ[j]*pow((pow(linef[j],2.0)-pow(f,2.0)),2.0));
      }
    }
  }
  
  int Nint;
  double *xint, *yint;
  
  Nint = (int)((fmax-fmin)*Tobs);
  xint = (double*)malloc(sizeof(double)*(Nint));
  yint = (double*)malloc(sizeof(double)*(Nint));
  for (i = 0; i < Nint; ++i)
  {
    xint[i] = fmin+(double)(i)/Tobs;
  }
  
  CubicSplineGSL(Nspline, pspline, dspline, Nint, xint, yint);
  
  k = (int)(fmin*Tobs)-1;
  sprintf(filename, "%s_burnin_psd.dat", ifo);
  outFile = fopen(filename,"w");
  for (i = 0; i < Nint+k; ++i)
  {
    if (i<k){
      fprintf(outFile,"%.15e %.15e\n", (double)(i)/Tobs, 1.);
    }
    else{
      fprintf(outFile,"%.15e %.15e\n", (double)(i)/Tobs, (Sn[i]+exp(yint[i-k]))/(Tobs/2.));
    }  
  }
  fclose(outFile);
  
  free(xint);
  free(yint);
  
  free(D);
  free(Draw);
  free(linew);
  free(sspecD);
  free(specD);
  free(Sn);
  free(freqs);
  free(sqf);
  
}

//TODO The Lorentzian model is defined in 3 different places!  That is not good!!
static void Lorentzian(double *Slines, double Tobs, lorentzianParams *lines, int ii, int N)
{
  int i;
  double f2,f4;
  double deltf;
  double freq,fsq, x, z, deltafmax, spread;
  double amplitude;
  int istart, istop, imid, idelt;
  
  double A = lines->A[ii];
  double Q = lines->Q[ii];
  double f = lines->f[ii];
  
  
  // here we figure out how many frequency bins are needed for the line profile
  imid = (int)((f)*Tobs);
  spread = (1.0e-2*Q);
  
  if(spread < 50.0) spread = 50.0;  // maximum half-width is f_resonance/50
  deltafmax = f/spread;
  deltf = 8.0*deltafmax;
  idelt = (int)(deltf*Tobs)+1;
  
  
  istart = imid-idelt;
  istop = imid+idelt;
  if(istart < 0)  istart = 0;
  if(istop > N/2) istop = N/2;
  
  
  
  // add or remove the old line
  f2=f*f;
  f4=f2*f2;
  amplitude = A*f4;//(f2*Q*Q);
  for(i=istart; i<istop; i++)
  {
    freq = (double)i/Tobs;
    fsq = freq*freq;
    x = fabs(f-freq);
    z = 1.0;
    if(x > deltafmax) z = exp(-(x-deltafmax)/deltafmax);
    
    //Slines[i] += z*amplitude/(fsq*(fsq-f2)*(fsq-f2));
    Slines[i] += z*amplitude/(f2*fsq+Q*Q*(fsq-f2)*(fsq-f2));
  }
  
}

static void gaussian_kernel_smoothing(double *data, int N, double sigma)
{
  int i,j;
  
  /* calculate smoothing kernel */
  int NK = (int)sigma*6*2+1; //size of Kernel (6 sigma either side of 0)
  int imid = (NK-1)/2;       //width of one side of kernel
  double K[NK+1];            //kernel

  for(i=0; i<=NK; i++)
  {
    K[i] = exp( -0.5 * (double)(i-imid) * (double)(i-imid)/sigma/sigma )/sqrt(2.*M_PI*sigma*sigma);
  }
  
  
  
  /* setup array for smoothed data */
  double *data_smoothed = malloc(N*sizeof(double));
  for(i=0; i<N; i++) data_smoothed[i] = data[i];
  
  /* convolve kernel with data */
  for(i=imid; i < N-imid; i++)
  {
    data_smoothed[i]=0.0;
    for(j=-imid; j<=imid; j++)
    {
      data_smoothed[i] += K[j+imid]*data[i+j] ;
    }
  }
  
  /* copy smoothed data into original array */
  for(i=0; i<N; i++) data[i] = data_smoothed[i];
  
  free(data_smoothed);
}

void BayesLineBurnin(struct BayesLineParams *bayesline, double *timeData, double *freqData, char *ifo)
{
  //Initialize BayesLine data structures
  BayesLineInitialize(bayesline);


  /*********************************************************************************************
   The pspline array holds the locations of the control points (knots) used in the spline
   The dspline array holds the values of the log of smooth spectrum at the control points
   The linef array contains the central frequencies of the Lorentzians
   The lineh array contains the amplitudes of the Lorentzians
   The lineQ array contains the Q factors of the Lorentzians
   *********************************************************************************************/

  //shortcuts to members of BayesLine structure
  dataParams *data           = bayesline->data;
  lorentzianParams *lines    = bayesline->lines_x;
  splineParams *spline       = bayesline->spline_x;

  int i,j;
  int max_lines=bayesline->maxBLLines;
  //number of samples in the data
  double Tobs = data->Tobs;
  double fmax = data->fmax;
  double dt   = 1./data->cadence;

  int N = 2*(int)floor(fmax*Tobs);
  blstart(timeData, freqData, N, dt, data->fmin, &spline->n, &lines->n, spline->data, spline->points, lines->f, lines->A, lines->Q, max_lines, ifo);

  //Work space for assembling PSD model
  double *f     = malloc(sizeof(double)*(N+1));
  double *logSn = malloc(sizeof(double)*(N+1));
  double *Sl    = malloc(sizeof(double)*(N+1));
  
  int imin = data->fmin*Tobs;

  
  for(i=0; i<N/2; i++)
  {
    f[i]     = (double)i/Tobs;
    logSn[i] = 1.0;
    Sl[i]    = 0.0;
    
    if(i<N/2-imin){
    bayesline->Sbase[i] = 1.0;
    bayesline->Sline[i] = 0.0;
    }
  }

  //interpolate cubic spline points
  CubicSplineGSL(spline->n,spline->points,spline->data,N/2-imin,f+imin,logSn+imin);

  //initialize work space for spline model
  bayesline->data->flow  = bayesline->data->fmin;

  for(i=0; i< bayesline->data->ncut; i++)
  {
    j = i + imin - bayesline->data->nmin;
    bayesline->power[j] = freqData[2*j]*freqData[2*j]+freqData[2*j+1]*freqData[2*j+1];
    bayesline->spow[i]  = bayesline->power[j];
    bayesline->sfreq[i] = bayesline->freq[j];
  }

  //form-up line model
  for(i=0; i<lines->n; i++)
  {
    lines->larray[i] = i;
    //printf("Line %i: %g %g\n",i,lines->f[i], lines->A[i]);
    Lorentzian(Sl, Tobs, lines, lines->larray[i], N);
  }
  
  //combine spline and line model
  for(i=0; i<N/2-imin; i++)
  {
    bayesline->Sbase[i] = exp(logSn[i+imin]);
    bayesline->Sline[i] = Sl[i+imin];
    bayesline->Snf[i] = bayesline->Sbase[i] + bayesline->Sline[i];
  }
  
  //Use initial estimate of PSD to set priors
  double PRIORSCALE = 10.0;
  /*
   set the lower bound of the frequency prior to be a factor of PRIORSCALE
   lower than ~max sensitivty in the "bucket"
   */
  double f_bucket = 200.0; //Hz
  int i_bucket = (int)floor(f_bucket*Tobs);
  for(i=0; i<N/2-imin; i++)
  {
    bayesline->priors->sigma[i] = bayesline->Snf[i];
    bayesline->priors->mean[i]  = bayesline->Snf[i];
    if(bayesline->flatPriorFlag) bayesline->priors->lower[i] = bayesline->Sbase[i_bucket-imin] / (PRIORSCALE*10);
    else                         bayesline->priors->lower[i] = bayesline->Sbase[i]/PRIORSCALE;
    //bayesline->priors->upper[i] = bayesline->Snf[i]   * PRIORSCALE;
    bayesline->priors->upper[i] = (bayesline->Sbase[i] + bayesline->Sline[i]*100.0)*PRIORSCALE;
  }
  
  //Smooth PSD prior
  gaussian_kernel_smoothing(bayesline->priors->upper, N/2-imin, 1);

  free(f);
  free(logSn);
  free(Sl);

}
