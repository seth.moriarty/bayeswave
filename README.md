# BayesWave

 **A Bayesian algorithm designed to robustly distinguish gravitational wave signals 
from noise and instrumental glitches without relying on any prior assumptions of waveform morphology.**

**BayesWave produces posterior probability distributions directly on gravitational wave 
signals and instrumental glitches, allowing robust, morphology-independent waveform reconstruction.**

See [arXiv:1410.3835](https://arxiv.org/abs/1410.3835) for an introduction to the BayesWave 
algorithm.

*LSC/Virgo members*: See the [BayesWave wiki](https://wiki.ligo.org/Bursts/BayesWave) for further info

## Citations

### Citing the scientific publications that describe the algorithms 
We request that
publications and other work arising from use of this code cite the following:
```
@ARTICLE{2015CQGra..32m5012C,
       author = {{Cornish}, Neil J. and {Littenberg}, Tyson B.},
        title = "{Bayeswave: Bayesian inference for gravitational wave bursts and
        instrument glitches}",
      journal = {Classical and Quantum Gravity},
     keywords = {General Relativity and Quantum Cosmology, Astrophysics - High Energy
        Astrophysical Phenomena, Astrophysics - Instrumentation and
        Methods for Astrophysics},
         year = 2015,
        month = Jul,
       volume = {32},
          eid = {135012},
        pages = {135012},
          doi = {10.1088/0264-9381/32/13/135012},
archivePrefix = {arXiv},
       eprint = {1410.3835},
 primaryClass = {gr-qc},
       adsurl = {https://ui.adsabs.harvard.edu/#abs/2015CQGra..32m5012C},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}

@article{PhysRevD.91.084034,
  title = {Bayesian inference for spectral estimation of gravitational wave detector noise},
  author = {Littenberg, Tyson B. and Cornish, Neil J.},
  journal = {Phys. Rev. D},
  volume = {91},
  issue = {8},
  pages = {084034},
  numpages = {13},
  year = {2015},
  month = {Apr},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevD.91.084034},
  url = {https://link.aps.org/doi/10.1103/PhysRevD.91.084034}
}

@article{Cornish:2020dwh,
    author = "Cornish, Neil J. and Littenberg, Tyson B. and B\'ecsy, Bence and Chatziioannou, Katerina and Clark, James A. and Ghonge, Sudarshan and Millhouse, Margaret",
    title = "{BayesWave analysis pipeline in the era of gravitational wave observations}",
    eprint = "2011.09494",
    archivePrefix = "arXiv",
    primaryClass = "gr-qc",
    doi = "10.1103/PhysRevD.103.044006",
    journal = "Phys. Rev. D",
    volume = "103",
    number = "4",
    pages = "044006",
    year = "2021"
}

```

### Citing the BayesWave Software
We also request that software developed from this code respository refers the URL of this project and indicates the code version (via e.g., tag and/or git hash) used for development.

## Contact

At this time, while the software is free to use, we cannot guarentee any 
particular level of direct support.  Interested individuals should direct questions to question@ligo.org
and we will strive to provide what assistance we can.

*LSC/Virgo members*:  bayeswave@sympa.ligo.org, burst@ligo.org

## Installation and operation
 * [Minimal quick-start guide](https://git.ligo.org/lscsoft/bayeswave/blob/master/doc/quickstart.md)
 * [Full Installation instructions](https://git.ligo.org/lscsoft/bayeswave/blob/master/doc/install.md)
 * [Running Analyses](https://git.ligo.org/lscsoft/bayeswave/blob/master/doc/running.md)

## Available Branches
There are two main branches (with corresponding singularity containers) in common use:
 * Master: the main branch with latest features, active development and little guarentee of 100% functionality.
 * Stable: functionally unchanged from O2.  Should be considered the *reviewed, production-level* code.

Older SVN branches and tags are also available.  Functionality is not
guarenteed, they have not been updated to work with the latest infrastructure
and are preserved purely for posterity.

## Contributing
Procedures for developing and contributing to this repository match those for
[lalsuite](https://git.ligo.org/lscsoft/lalsuite).  The contribution guide for
lalsuite can be found
[here](https://git.ligo.org/lscsoft/lalsuite/blob/master/CONTRIBUTING.md)

Documentation is most conveniently provided via markdown (.md) files in the
repository itself.  The page you are now reading is the `README.md` at the root
of the master branch.  All other documentation should live in `doc`.

**TLDR**
 * We use the fork/merge development model
 * Development projects should take place on feature branches (*NOT* adhoc
   sub-directories!)

This eliminates conflicts, ensures the master branch is always useable (no one
breaks the build etc) and means different features/branches are easily
found, used and contributed to.

So: 
 1. create your own fork of this repository
 1. create a branch to develop some feature on the fork
 1. make and commit changes
 1. push that branch upstream
 1. when development is complete, merge that branch's changes back into master

Full details of how to do this can be found in the [lalsuite contribution
guide](https://git.ligo.org/lscsoft/lalsuite/blob/master/CONTRIBUTING.md)

This is, first and foremost, a *source* repository.  To reduce bloat and
simplify development, data products and analysis-specific scripts/configuration
files should really be hosted in separate repositories or archives which
document those analyses.

## Repository Structure
This repository has been cloned from the original svn repository at [
https://svn.ligo.caltech.edu/svn/bayeswave](
https://svn.ligo.caltech.edu/svn/bayeswave).  Commit history, branches and tags
have been preserved and non-source directories have been excised from the
history.  Some details of the svn->git transition can be found
[here](https://wiki.ligo.org/Bursts/BayesWave/BayesWaveGit).

The git master branch looks like:
* `src`: The main source directory.  Includes all C code, header files and build tools
* `test`: Test scripts to check successful compilation and linking.
* `utils`: Python and bash utility scripts.
* `postprocess`: Python and bash post-processing (e.g., plots, webpages) scripts
* `BayesWavePipe`: A standalone python module for HTCondor workflow generation

Note that different branches in the original svn do not share a uniform
structure; if you cannot find the directory or file you're looking for, check
you're on the correct branch.



