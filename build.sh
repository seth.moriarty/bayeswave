#!/bin/bash
set -e

INSTALL_PREFIX=$1

# Get cmake
if hash cmake 2>/dev/null; then
  CMAKE=cmake

  # Check version >2
  CMAKE_VERSION=$(${CMAKE} --version | head -1 | awk '{print $3}')
  CMAKE_VERSION=( ${CMAKE_VERSION//./ } )
  CMAKE_MAJOR_VERSION=${CMAKE_VERSION[0]}

  if [ ${CMAKE_MAJOR_VERSION} -lt 3 ]; then
    echo "cmake executable is version ${CMAKE_VERSION}, checking for cmake3 executable..."
    if hash cmake3 2> /dev/null; then
      CMAKE=cmake3
    else 
      echo "cmake3 not found"
      exit 1
    fi
  fi
elif hash cmake3 2>/dev/null; then
  CMAKE=cmake3
else
  echo "No cmake detected.  BayesWave requires cmake version 3."
  exit 1
fi
${CMAKE} --version

rm -rf build
mkdir -p build
pushd build
${CMAKE} .. \
	-DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_EXPORT_COMPILE_COMMANDS=true
${CMAKE} --build . -- VERBOSE=1
${CMAKE} --build . --target install
popd
